import matplotlib.pyplot as plt
import numpy as np
import math



           #-->12: (2 little, 1 big)             //pour voir si les littles mènent la danse


"""
number_of_cpus = ["1_p-Big_stp","1-Big_stp", "1-0","0-1_b","0-1_B"]
workload = [ 1.45, 1.45, 1.55, 7.03,  7.69 ]
energy_consumed = [ 33.7, 34.4,  33.3, 19.4,  26.1  ]
"""


""" result from google pixel
number_of_cpus = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0" , "5-0.", "6-0", "6-0.",                          "6-1",    "6-2"             ,"0-1", "0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29285.405, 38598.14, 46077.78,    53629.51, 58756.40, 56820.57,   66053.62,  64353.13,                 106210.78 , 123823.21,       66877.5,115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15,    1661.97,   1918.21,   1871.11,                2970.56 ,  3397.90,          1938.45,3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.55,    3.368,  4.9563   ,     6.906,  7.3213,     9.223199,   10.85176,   11.1178,                       17.08829 ,  21.384681,       6.7442, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          
"""




                                                                                                                

number_of_cpus = ["mouse",  "idle" ,               "1-0",    "2-0",      "3-0",            "4-0",      "4-1",   "4-2",      "4-3",    "4-4",             "0-1",     "0-2",    "0-3" ,   "0-4",                  "2-1",   "3-1" ,    "1-1",                         "1-2", "1-3"       ]                                                        
phone_energy= [ 1308,     15437.75 ,         51208.79 , 54347.47,  57513.95   ,       74701.21,       98200.09 , 124222.42, 128746.44  , 145726.86,      94219.85 , 100720.96,  141336.4,   122949.4 ,         98711.42,  101212.13 ,  90216.11,                    110737,  111186.62    ]                                                                                                   
phone_power = [ 40.16,    1047.42   ,         2617.20, 2532.40 ,  1950.01   ,         2618.09,       2620.45 ,     2616.48,     38.35,  2620.32,             2620.1  , 2622.43,     2231.52 , 2623.83,                2619.02, 2620.84 ,   2620.50,                2621.93,   2618.84      ]                  
workload  = [ 0.1,        1 ,                1.25068,  2.688753,   4.02600  ,          5.25640  ,   8.44232736,    11.73684641,  13.9001248561 , 16.270943,    3.61959474 , 6.09298,  10.06644613, 11.9685747498 ,           6.14819, 7.27350446993,   4.550717,       7.632924365,   10.042245486   ]          


""" 
original arrays before permutations
number_of_cpus = ["mouse",  "idle" ,               "1-0",    "2-0",      "3-0",            "4-0",       "0-1",     "0-2",    "0-3" ,   "0-4",         "4-1",   "4-2",      "4-3",     "4-4",                          "1-1", "2-1",   "3-1",          ]     #  "1-2", "1-3",       ]                                                        
phone_energy= [ 1308,     15437.75 ,         51208.79 , 54347.47,  57513.95   ,       74701.21,    94219.85 , 100720.96,  141336.4,   122949.4 ,        98200.09 , 162361.88, 128746.44  , 145726.86,             90216.11, 98711.42,  101212.13    ]                                                                                                   
phone_power = [ 40.16,    1047.42   ,         2617.20, 2532.40 ,  1950.01   ,         2618.09,    2620.1  , 2622.43,     2231.52 , 2623.83,              2620.45 ,  1918.21,     38.35,   2620.32,                         2620.50, 2619.02, 2620.84            ]                  
workload  = [ 0.1,        1 ,                1.25068,  2.688753,   4.02600  ,          5.25640  ,  3.61959474 , 6.09298,  10.06644613, 11.9685747498 ,     8.44232736,   13.02976, 13.9001248561 , 16.270943,            4.550717,  6.14819,   7.27350446993   ]          
"""





fig = plt.figure()

width = 0.35  
plt.bar(number_of_cpus,phone_power, width, label='With new usb cable')

# Add title and axis names
plt.title('Avg power used by the phone according to the configuration ')
plt.xlabel('Number of CPUs')
plt.ylabel('AVG Power')

#plt.xticks(fontsize=8)
fig.autofmt_xdate()

plt.savefig("Samsung_galaxy_s8_power_absorbed_according_to_configuration.png")
plt.legend(loc='upper left')
plt.clf()
plt.cla()
plt.close()
########################################################################################

fig = plt.figure()
plt.bar(number_of_cpus,phone_energy, width=0.4)
 
# Add title and axis names
plt.title('Energy consumed according to the \n number of configuration')
plt.xlabel('Number of CPUs')
plt.ylabel('Battery cpu usage')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Samsung_galaxy_s8_energy_usage_according_to_configuration.png")

plt.clf()
plt.cla()
plt.close()

#################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(workload,100000000000), width=0.4)

# Add title and axis names
plt.title('New computed Workload according to the configuration')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Workload ($\times 10E11$)')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Samsung_galaxy_s8_workload_according_to_configuration.png")

plt.clf()
plt.cla()
plt.close()
################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy,workload), width=0.4)
# Add title and axis names
plt.title('Energy efficiency Energy/Workload according to \n the configuration (lower is better)')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Battery cpu/Workload ($\times 10E-11$)')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Samsung_galaxy_s8_ratio_energy_by_workload_according_to_configuration.png")

plt.clf()
plt.cla()
plt.close()

################################################


