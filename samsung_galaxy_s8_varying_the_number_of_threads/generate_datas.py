from ctypes import sizeof
import matplotlib.pyplot as plt
import numpy as np
import math
import os



"""
configurations = ["1_p-Big_stp","1-Big_stp", "1-0","0-1_b","0-1_B"]
workload = [ 1.45, 1.45, 1.55, 7.03,  7.69 ]
energy_consumed = [ 33.7, 34.4,  33.3, 19.4,  26.1  ]
"""


""" result from google pixel
configurations = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0" , "5-0.", "6-0", "6-0.",                          "6-1",    "6-2"             ,"0-1", "0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29285.405, 38598.14, 46077.78,    53629.51, 58756.40, 56820.57,   66053.62,  64353.13,                 106210.78 , 123823.21,       66877.5,115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15,    1661.97,   1918.21,   1871.11,                2970.56 ,  3397.90,          1938.45,3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.55,    3.368,  4.9563   ,     6.906,  7.3213,     9.223199,   10.85176,   11.1178,                       17.08829 ,  21.384681,       6.7442, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          
"""



""" 
original arrays before permutations
configurations = ["mouse",  "idle" ,               "1-0",    "2-0",      "3-0",            "4-0",       "0-1",     "0-2",    "0-3" ,   "0-4",         "4-1",   "4-2",      "4-3",     "4-4",                          "1-1", "2-1",   "3-1",          ]     #  "1-2", "1-3",       ]                                                        
phone_energy= [ 1308,     15437.75 ,         51208.79 , 54347.47,  57513.95   ,       74701.21,    94219.85 , 100720.96,  141336.4,   122949.4 ,        98200.09 , 162361.88, 128746.44  , 145726.86,             90216.11, 98711.42,  101212.13    ]                                                                                                   
phone_power = [ 40.16,    1047.42   ,         2617.20, 2532.40 ,  1950.01   ,         2618.09,    2620.1  , 2622.43,     2231.52 , 2623.83,              2620.45 ,  1918.21,     38.35,   2620.32,                         2620.50, 2619.02, 2620.84            ]                  
workload  = [ 0.1,        1 ,                1.25068,  2.688753,   4.02600  ,          5.25640  ,  3.61959474 , 6.09298,  10.06644613, 11.9685747498 ,     8.44232736,   13.02976, 13.9001248561 , 16.270943,            4.550717,  6.14819,   7.27350446993   ]          
"""



                                                                                                                
""" final values
configurations = ["mouse",  "idle" ,               "1-0",    "2-0",      "3-0",            "4-0",      "4-1",   "4-2",      "4-3",    "4-4",             "0-1",     "0-2",    "0-3" ,   "0-4",                  "2-1",   "3-1" ,    "1-1",                         "1-2", "1-3"       ]                                                        
phone_energy= [ 1308,     15437.75 ,         51208.79 , 54347.47,  57513.95   ,       74701.21,       98200.09 , 124222.42, 128746.44  , 145726.86,      94219.85 , 100720.96,  141336.4,   122949.4 ,         98711.42,  101212.13 ,  90216.11,                    110737,  111186.62    ]                                                                                                   
phone_power = [ 40.16,    1047.42   ,         2617.20, 2532.40 ,  1950.01   ,         2618.09,       2620.45 ,     2616.48,     38.35,  2620.32,             2620.1  , 2622.43,     2231.52 , 2623.83,                2619.02, 2620.84 ,   2620.50,                2621.93,   2618.84      ]                  
workload  = [ 0.1,        1 ,                1.25068,  2.688753,   4.02600  ,          5.25640  ,   8.44232736,    11.73684641,  13.9001248561 , 16.270943,    3.61959474 , 6.09298,  10.06644613, 11.9685747498 ,           6.14819, 7.27350446993,   4.550717,       7.632924365,   10.042245486   ]          
"""









# I used the final datas without the mouse résult
configurations = ["idle" ,               "1-0",    "2-0",      "3-0",            "4-0",      "4-1",   "4-2",      "4-3",    "4-4",             "0-1",     "0-2",    "0-3" ,   "0-4",                  "2-1",   "3-1" ,    "1-1",                         "1-2", "1-3"       ]                                                        
phone_energy= [  15437.75 ,         51208.79 , 54347.47,  57513.95   ,       74701.21,       98200.09 , 124222.42, 128746.44  , 145726.86,      94219.85 , 100720.96,  141336.4,   122949.4 ,         98711.42,  101212.13 ,  90216.11,                    110737,  111186.62    ]                                                                                                   
phone_power = [   1047.42   ,         2617.20, 2532.40 ,  1950.01   ,         2618.09,       2620.45 ,     2616.48,     38.35,  2620.32,             2620.1  , 2622.43,     2231.52 , 2623.83,                2619.02, 2620.84 ,   2620.50,                2621.93,   2618.84      ]                  
workload  = [  1 ,                1.25068,  2.688753,   4.02600  ,          5.25640  ,   8.44232736,    11.73684641,  13.9001248561 , 16.270943,    3.61959474 , 6.09298,  10.06644613, 11.9685747498 ,           6.14819, 7.27350446993,   4.550717,       7.632924365,   10.042245486   ]          
workload = np.multiply(workload,10**11)  # Note !!! In previous machine learning models I did not realize this multiplication. 
                                         # It is mandatory to restart generation process if I need to compare with automatic experiment, in which I printed the exact value of workload. 
phone_energy=np.divide(phone_energy,10**3)

cwd = os.getcwd()
print ("current directory = ")
print(cwd)
dir = os.path.join(cwd,"machine_learning_datas")
if not os.path.exists(dir):
    os.mkdir(dir)

with open('machine_learning_datas/configurations_user_friendly.txt','w') as file:
    print("--- writing the phone configurations in user friendly format")
    counter = 1
    for value in configurations:
            file.write(str(value))
            if (counter < len(configurations)):
                file.write('\n')
                counter = counter + 1



with open('machine_learning_datas/configurations_generic_format.txt','w') as file:
    print("--- writing the configurations for generic phone in format (l,l,l,l,l,l,m,m,m,b,b,b,b)")
    #for generic phone, we have 6 maximum little cores, 3 maximum medium cores and 4 maximum big cores
    # for the frequency we consider that 1 means minimum or idle, 2 means half frequency, and 3 means maximum frequency
    # On this datas we vary the number of thread, for this reason all value are 2, supposing that cpu running the thread are on maximum frequency. 
    # For now we don't add equivalent values, we will add them later if possible, but we consider that on the same socket the core on which the thread is pinned does not matter. 
    # for exemple  (3,0,0,0,0,0, 0,0,0, 0,0,0,0), # could be equivalent to  (0,3,0,0,0,0, 0,0,0, 0,0,0,0)
    """
    configurations = ["idle" ,               
    "1-0",    "2-0",      "3-0",            
    "4-0",      "4-1",   "4-2",      
    "4-3",    "4-4",             
    "0-1",     "0-2",    "0-3" ,   "0-4",                  
    "2-1",   "3-1" ,    "1-1",                         
    "1-2", "1-3"       ]                                                                                             
    """
    file.write("0,0,0,0,0,0, 0,0,0, 0,0,0,0\n" 
        "3,0,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,3,0,0,0, 0,0,0, 0,0,0,0\n"
        
        "3,3,3,3,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,3,3,0,0, 0,0,0, 3,0,0,0\n"
        "3,3,3,3,0,0, 0,0,0, 3,3,0,0\n"
        
        "3,3,3,3,0,0, 0,0,0, 3,3,3,0\n"
        "3,3,3,3,0,0, 0,0,0, 3,3,3,3\n"

        "0,0,0,0,0,0, 0,0,0, 3,0,0,0\n"
        "0,0,0,0,0,0, 0,0,0, 3,3,0,0\n"
        "0,0,0,0,0,0, 0,0,0, 3,3,3,0\n"
        "0,0,0,0,0,0, 0,0,0, 3,3,3,3\n"

        "3,3,0,0,0,0, 0,0,0, 3,0,0,0\n"
        "3,3,3,0,0,0, 0,0,0, 3,0,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 3,0,0,0\n"

        "3,0,0,0,0,0, 0,0,0, 3,3,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 3,3,3,0\n"
        )

with open('machine_learning_datas/configuration_google_pixel_format.txt','w') as file:
    print("--- writing the configuration for google pixel in format (l,l,l,l,l,l,m,b)")
    #  we have 4 maximum little cores, 4  big cores"
    # we also have same considerations as above with the generic configuration
    file.write("0,0,0,0,0,0,0,0\n" 
        "3,0,0,0,0,0,0,0\n"
        "3,3,0,0,0,0,0,0\n"
        "3,3,3,0,0,0,0,0\n"
        
        "3,3,3,3,0,0,0,0\n"
        "3,3,3,3,3,0,0,0\n"
        "3,3,3,3,3,3,0,0\n"
        
        "3,3,3,3,3,3,3,0\n"
        "3,3,3,3,3,3,3,3\n"

        "0,0,0,0,3,0,0,0\n"
        "0,0,0,0,3,3,0,0\n"
        "0,0,0,0,3,3,3,0\n"
        "0,0,0,0,3,3,3,3\n"

        "3,3,0,0,3,0,0,0\n"
        "3,3,3,0,3,0,0,0\n"
        "3,0,0,0,3,0,0,0\n"

        "3,0,0,0,3,3,0,0\n"
        "3,0,0,0,3,3,3,0\n"
        )


with open('machine_learning_datas/phone_energy.txt','w') as file:
    print("--- writing the phone energy")
    counter = 1
    for value in phone_energy:
            file.write(str(value))
            if (counter < len(phone_energy)):
                file.write('\n')
                counter = counter + 1

with open('machine_learning_datas/phone_power.txt','w') as file:
    print("--- writing the phone power")
    counter = 1
    for value in phone_energy:
        file.write(str(value))
        if (counter < len(phone_energy)):
            file.write('\n')
            counter = counter + 1

with open('machine_learning_datas/workload_computed.txt','w') as file:
    print("--- writing the workload comuted")
    counter = 1
    for value in workload:
        file.write(str(value))
        if (counter < len(workload)):
            file.write('\n')
            counter = counter + 1
    

with open('machine_learning_datas/ratio_energy_by_worload.txt','w') as file:
    print("--- writing the ratio energy by workload")
    ratio = np.divide(phone_energy,workload)
    counter = 1
    for value in ratio:
        file.write(str(value))
        if (counter < len(ratio)):
            file.write('\n')
            counter = counter + 1
  




 
configurations_for_automatisation = [[0,0,0,0,0,0,0,0],
        [3,0,0,0,0,0,0,0],
        [3,3,0,0,0,0,0,0],
        [3,3,3,0,0,0,0,0],
        
        [3,3,3,3,0,0,0,0],
        [3,3,3,3,3,0,0,0],
        [3,3,3,3,3,3,0,0],
        
        [3,3,3,3,3,3,3,0],
        [3,3,3,3,3,3,3,3],

        [0,0,0,0,3,0,0,0],
        [0,0,0,0,3,3,0,0],
        [0,0,0,0,3,3,3,0],
        [0,0,0,0,3,3,3,3],

        [3,3,0,0,3,0,0,0],
        [3,3,3,0,3,0,0,0],
        [3,0,0,0,3,0,0,0],

        [3,0,0,0,3,3,0,0],
        [3,0,0,0,3,3,3,0]]
configuration_human_readable = ["0000-0000",
        "3000-0000",
        "3300-0000",
        "3330-0000",
        
        "3333-0000",
        "3333-3000",
        "3333-3300",
        
        "3333-3330",
        "3333-3333",

        "0000-3000",
        "0000-3300",
        "0000-3330",
        "0000-3333",

        "3300-3000",
        "3330-3000",
        "3000-3000",

        "3000-3300",
        "3000-3330"]

data_transposed = []
data_transposed.append(configuration_human_readable)
data_transposed.append(configurations_for_automatisation)


data = (np.array(data_transposed, dtype=object)).T

header = ["configurations", "samsung galaxy format"]
print ("final data", data)

with open('machine_learning_datas/input_configurations_file.csv','w') as file:
    counter = 1
    for title in header:
        file.write(title)
        if (counter < len(header)):
            file.write(',')
            counter = counter + 1
    file.write('\n')
    
    line = []
    for line in data:
        counter_value = 1
        for value in line:
            file.write(str(value).replace(",", "-"))
            if (counter_value < len(line)):
                file.write(',')
                counter_value = counter_value + 1
        file.write('\n')
# for machine learning process I did not use the "mouse" results





"""
txt = ["PS5", "is", "currently", "unavailable", "in", "India"]

with open('shows.csv','w') as file:
    for line in txt:
        file.write(line)
        file.write('\n')

plt.bar(configurations,phone_power, width, label='With new usb cable')


plt.bar(configurations,phone_energy, width=0.4)

#################################################


plt.bar(configurations,np.divide(workload,100000000000), width=0.4)


plt.bar(configurations,np.divide(phone_energy,workload), width=0.4)

"""

