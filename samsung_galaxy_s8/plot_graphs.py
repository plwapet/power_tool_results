import matplotlib.pyplot as plt
import numpy as np
import math



           #-->12: (2 little, 1 big)             //pour voir si les littles mènent la danse


"""
number_of_cpus = ["1_p-Big_stp","1-Big_stp", "1-0","0-1_b","0-1_B"]
workload = [ 1.45, 1.45, 1.55, 7.03,  7.69 ]
phone_energy = [ 33.7, 34.4,  33.3, 19.4,  26.1  ]
"""



number_of_cpus =                               ["mouse",    "idle"   , "1-0",    "0-1",     ]
phone_energy_considering_only_cc_info       = [ 1308,     2000   ,    34000   ,    85500    ]
phone_energy_considering_cc_info_variations = [ 1308,        15437.75 ,   64307.19   ,    114213.37    ]
phone_energy_cc_info_hv_charger_stop = [1308,          11132.24,          29802.83,       36050.37 ]
phone_energy =                              [ 1308,         35437.75    ,   79807.19   ,    92713.37    ]
phone_power =                                 [ 40.16,        1047.42   ,    2285.79   ,      2634.62   ] 
workload =                                      [ 0.1,              1,   1.2568      ,  4.00460809  ] # 0.1 are not computed values


'''
# for the power point
number_of_cpus =                               [ "idle"   , "1-0",    "0-1",     ]
phone_energy_considering_only_cc_info       = [ 2000   ,  34000   ,    85500    ]
phone_energy_considering_cc_info_variations = [ 15437.75    ,   64307.19   ,    114213.37    ]
phone_energy =                              [ 35437.75    ,   79807.19   ,    92713.37    ]
phone_power =                                 [ 1047.42   ,    2285.79   ,      2634.62   ] 
workload =                                      [  1,   1.2568      ,  4.00460809  ] # 0.1 are not computed values
'''






fig = plt.figure()
plt.bar(number_of_cpus,phone_power, width=0.4)
 
# Add title and axis names
plt.title('Avg power used by the phone according to  configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Configuration')
plt.ylabel('AVG Power')

plt.savefig("Samsung_Gal_S8_power_absorbed_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()


fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy, 1000), width=0.4)
 
# Add title and axis names
plt.title('Energy consumed by the phone according to the thread configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Configuration')
plt.ylabel(r'Energy consumed ($\times 10E3 mAh$)')
#fig.autofmt_xdate()
plt.savefig("Samsung_Gal_S8_energy_consumed_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()

#################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(workload,100000000000), width=0.4)

# Add title and axis names
plt.title('New computed Workload according to the number of CPUs  \n BM = Battery at middle level (50%)')
plt.xlabel('Configuration')
plt.ylabel(r'Workload ($\times 10E11$)')

plt.savefig("Samsung_Gal_S8_workload_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()
################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy,workload), width=0.4)
# Add title and axis names
plt.title('Energy/ Workload according to the number of CPUs  \n BM = Battery at middle level (50%)')
plt.xlabel('Configuration')
plt.ylabel(r'Energy consumed/Workload ($\times 10E-11$)')

plt.savefig("Samsung_Gal_S8_ratio_energy_by_workload_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()

################################################
#When taking into account cc_info variations,




'''
fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy_considering_cc_info_variations, 1000), width=0.4)
 
# Add title and axis names
'''
'''
plt.title('Energy consumed according to the configuration.', fontsize=15)
plt.xlabel('Configuration', fontsize=15) 
plt.ylabel(r'Energy consumed (Ah)',  fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
'''
'''

plt.title('Energy consumed according to the configuration.')
plt.xlabel('Configuration') 
plt.ylabel(r'Energy consumed (Ah)')

'''

###
x = np.arange(len(number_of_cpus))  # the label locations
width = 0.35  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, phone_energy_considering_cc_info_variations , width, label='Energy consumed with cc_info variations')
rects2 = ax.bar(x + width/2, phone_energy_cc_info_hv_charger_stop , width, label='Energy consumed when cc_info is constant (hv_charger_stop = 0)')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_xlabel('Tested configuration')
ax.set_ylabel('Consumed energy')
ax.set_title('Avg energy used by the phone according to the configuration ')
ax.set_xticks(x, number_of_cpus)
ax.legend()

ax.bar_label(rects1, padding=3)
ax.bar_label(rects2, padding=3)

fig.tight_layout()
###

plt.savefig("Samsung_Gal_S8_energy_consumed_according_to_cpu_with_cc_info_variations.png")

plt.clf()
plt.cla()
plt.close()

#####################################################################""
fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy_considering_cc_info_variations,workload), width=0.4)
# Add title and axis names
plt.title('Energy efficiency (Energy/Workload) according to the configuration')
plt.xlabel('Configuration')
plt.ylabel(r'Energy/Workload ($\times 10E-11$)')

plt.savefig("Samsung_Gal_S8_ratio_energy_by_workload_according_to_cpu_with_cc_info_variations.png")

plt.clf()
plt.cla()
plt.close()

#############################################################################
#When considering  only cc_info variations, \n 

fig = plt.figure()
plt.bar(number_of_cpus,phone_energy_considering_only_cc_info, width=0.4)
 
# Add title and axis names
plt.title(' Energy consumed by the phone according to the thread configuration')
plt.xlabel('Configuration')
plt.ylabel('Energy consumed considering cc_info')

plt.savefig("Samsung_Gal_S8_energy_consumed_according_to_cpu_only_cc_info_variations.png")

plt.clf()
plt.cla()
plt.close()

#####################################################################""
fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy_considering_only_cc_info,workload), width=0.4)
# Add title and axis names
plt.title('Energy efficiency (Energy/Workload) according to the number of CPUs')
plt.xlabel('Configuration')
plt.ylabel(r'Energy/Workload ($\times 10E-11$)')

plt.savefig("Samsung_Gal_S8_ratio_energy_by_workload_according_to_cpu_only_cc_info_variations.png")

plt.clf()
plt.cla()
plt.close()


""" to delete
fig = plt.figure()

plt.plot(indices, batt_capacity_max, label = "cc_info", linestyle="-")
plt.plot(indices,  cc_info , label = "batt_capacitiy_max", linestyle="--")
plt.plot(indices, interface , label = "interface", linestyle="-.")


# Add title and axis names
plt.title('Evolution of battery description file contents, the UI battery level')
plt.xlabel('Measurements (the first 2 were taken well before the last ones) ')


plt.legend()
plt.savefig("Samsung_Gal_S8_investigating_batt_capacity_max.png")

plt.clf()
plt.cla()
plt.close()
###############################
"""



""" Plot used to analyse cc_info file

indices = [1,2,3,4,5,6,7,8, 9, 10, 11, 12, 13,14,15,16,17, 18, 19, 20, 21, 22]
batt_capacity_max = [700, 700, 700, 700, 700, 739, 772, 809, 835, 858, 877, 907, 919, 930, 941, 957,975, 988, 990, 990, 990, 990 ]
interface = [29, 29, 88, 97 ,100, 100, 100, 100, 100, 100, 100, 100, 100,100,100,100, 100, 100, 100, 100, 100, 100 ]
cc_info = [1275000, 1280000, 2525000, 2714000, 2810000, 2902500, 3028500, 3113000, 3182000, 3244000, 3301000, 3388000, 3421000, 3451500, 3484500, 3526500, 3553500, 3574500, 3593000, 3609500, 3629500, 3631500]






fig, ax = plt.subplots()

# Plot linear sequence, and set tick labels to the same color
line_1 = ax.plot(indices , interface , color='red', label = "UI_battery_level", linestyle="-")
ax.tick_params(axis='y', labelcolor='red')



ax2 = ax.twinx()

# Plot exponential sequence, set scale to logarithmic and change tick color
line_2 = ax2.plot(indices,  cc_info, color='green', label = "cc_info", linestyle="--")
ax2.tick_params(axis='y', labelcolor='green')


lines = line_1+line_2
labs = [l.get_label() for l in lines]
ax.legend(lines, labs, loc=0)



# Add title and axis names
plt.title('Evolution cc_info and of the the UI battery level')
plt.xlabel('Measurements (the first 2 were taken well before the last ones) ')

plt.savefig("Samsung_Gal_S8_investigating_cc_info_user_battery_level.png")

plt.clf()
plt.cla()
plt.close()


##################################

fig, ax = plt.subplots()

# Plot linear sequence, and set tick labels to the same color
line_3 = ax.plot(indices , batt_capacity_max, color='red', label = "batt_capacitiy_max", linestyle="-")
ax.tick_params(axis='y', labelcolor='red')


ax2 = ax.twinx()

# Plot exponential sequence, set scale to logarithmic and change tick color
line_4 = ax2.plot(indices,  cc_info, color='green', label = "cc_info", linestyle="--")
ax2.tick_params(axis='y', labelcolor='green')





lines = line_3+line_4
labs = [l.get_label() for l in lines]
ax.legend(lines, labs, loc=0)


# Add title and axis names
plt.title('Evolution of cc_info and batt_capacity_max')
plt.xlabel('Measurements (the first 2 were taken well before the last ones) ')



plt.savefig("Samsung_Gal_S8_investigating_cc_info_batt_capacity_max.png")

plt.clf()
plt.cla()
plt.close()

"""