import matplotlib.pyplot as plt
import numpy as np
import math



           #-->12: (2 little, 1 big)             //pour voir si les littles mènent la danse




"""
number_of_cpus = ["1_p-Big_stp","1-Big_stp", "1-0","0-1_b","0-1_B"]
workload = [ 1.45, 1.45, 1.55, 7.03,  7.69 ]
phone_energy = [ 33.7, 34.4,  33.3, 19.4,  26.1  ]



number_of_cpus = ["mse",  "idl" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",         "3m-0",  "3h-0"  , "3-0",        "1-1m" , "1-1h", "1m-1m", "1h-1h", "1-1"                "0-1m",   "0-1h",    "0-1-0"   ,"0-0-1",                                    "0-2m",  "0-2h" ,               "0-11m",  "0-11h",            "0-2"    ]                                                        
phone_energy= [ 1308, 11047.73,             22554.99,  24785.02,  29197.24,               24277.3,   28502.31 , 38598.14,        26060.77, 32111.43,  46077.78,     35087.51, 47102.84,  28158.18 , 42742.2,          30052.34 ,    47133.62,  66877.5, 77632.30 ,                            35683.20, 67647.63,            80676.10 ,  98665.02 ,                 115136.69   ]                                                                                                        
phone_power = [ 40.16, 336.07,               679.99,  746.01, 876.80,                      729.67,   854.14    , 1066.77,      783.20,  960.98,  1272.45,           1045.80, 1389.07,   845.63,   1265.01,           899.02,       1390.60,   1938.45, 2214.51  ,                         1062.76,  1958.13 ,          2298.82,   2758.98 ,                      3179.93   ]                                   
workload  = [ 0.1,         0.9,               0.6206,   1.267, 1.60362534,                 1.41737,  2.61014   ,3.368042,        2.291607, 4.07984,  4.9563,       3.810539, 6.249128,    2.790242, 6.12180,          2.5540681,    5.53843,  6.7442,  7.645522240   ,                   4.5668,   10.317,          9.392402,    12.280638,       13.21045181    ]          



"""


plt.rcParams.update({'mathtext.default':  'regular' })
                                                                 # datas of 1-0, 2-0 , 0-1, 0-2 comes from previous experiments, 0-1 is when the thread si on the Big core not the medium one, and it is the same thing for 1-1
number_of_cpus = ["mse",  "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",         "3m-0",  "3h-0"  , "3-0",        "1-1m" , "1-1h", "1m-1m", "1h-1h",   "1-1",                "0-1m",   "0-1h",  "0-1-0"  , "0-0-1",                                    "0-2m",  "0-2h" ,               "0-11m",  "0-11h",            "0-2"    ]                                                        
phone_energy= [ 1308, 11047.73,             22554.99,  24785.02,  29197.24,               24277.3,   28502.31 , 38598.14,        26060.77, 32111.43,  46077.78,     35087.51, 47102.84,  28158.18 , 42742.2,  73048.75,         30052.34 ,   47133.62, 66877.5,  77632.30 ,                            35683.20, 67647.63,            80676.10 ,  98665.02 ,                 115136.69   ]                                                                                                        
phone_power = [ 40.16, 336.07,               679.99,  746.01, 876.80,                      729.67,   854.14    , 1066.77,      783.20,  960.98,  1272.45,           1045.80, 1389.07,   845.63,   1265.01, 2106.08,            899.02,       1390.60, 1938.45,   2214.51  ,                         1062.76,  1958.13 ,          2298.82,   2758.98 ,                      3179.93   ]                                   
workload  = [ 0.1,         0.9,               0.6206,   1.267, 1.60362534,                 1.41737,  2.61014   , 3.368042,        2.291607, 4.07984,  4.9563,       3.810539, 6.249128,    2.790242, 6.12180,   8.47040,         2.5540681,    5.53843,  6.7442,   7.645522240   ,                   4.5668,   10.317,          9.392402,    12.280638,       13.21045181    ]          

"""
number_of_cpus = ["mse",  "idl" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",         "3m-0",  "3h-0"  , "3-0",        "1-1m" , "1-1h", "1m-1m", "1h-1h",  "1-1",              "0-1m",   "0-1h",  "0-1-0"  , "0-0-1",                                     "0-2m",  "0-2h" ,               "0-11m",  "0-11h",            "0-2"    ]                                                        
phone_energy= [ 1308, 11047.73,             22554.99,  24785.02,  29197.24,               24277.3,   28502.31 , 38598.14,        26060.77, 32111.43,  46077.78,     35087.51, 47102.84,  28158.18 , 42742.2, 73048.75,         30052.34 ,    47133.62, 66877.5, 77632.30 ,                            35683.20, 67647.63,            80676.10 ,  98665.02 ,                 115136.69   ]                                                                                                        
phone_power = [ 40.16, 336.07,               679.99,  746.01, 876.80,                      729.67,   854.14    , 1066.77,      783.20,  960.98,  1272.45,           1045.80, 1389.07,   845.63,   1265.01, 2106.08,          899.02,       1390.60,   1938.45, 2214.51  ,                         1062.76,  1958.13 ,          2298.82,   2758.98 ,                      3179.93   ]                                   
workload  = [ 0.1,         0.9,               0.6206,   1.267, 1.60362534,                 1.41737,  2.61014   , 3.368042,        2.291607, 4.07984,  4.9563,       3.810539, 6.249128,    2.790242, 6.12180,  8.47040,        2.5540681,    5.53843,    6.7442, 7.645522240 ,                   4.5668,   10.317,          9.392402,    12.280638,       13.21045181    ]          
"""







fig = plt.figure()
plt.bar(number_of_cpus,phone_power, width=0.4)
 
# Add title and axis names
plt.title('Avg power used by the phone according to the thread configuration \n m = idle (minimum) frequency, h = half frequence ')
plt.xlabel('Number of CPUs')
plt.ylabel('AVG Power')
plt.xticks(fontsize=8)


fig.autofmt_xdate()
plt.savefig("Google_pixel_power_absorbed_according_to_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()


fig = plt.figure()
plt.bar(number_of_cpus,phone_energy, width=0.4)
 
# Add title and axis names
plt.title('Energy consumed by the phone according to the thread configuration \n  m = idle (minimum) frequency, h = half frequence')
plt.xlabel('Number of CPUs')
plt.ylabel('Energy consumed')
plt.xticks(fontsize=8)

fig.autofmt_xdate()
plt.savefig("Google_pixel_energy_consumed_according_to_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()

#################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(workload,100000000000), width=0.4)

# Add title and axis names
plt.title('New computed Workload according to the number of CPUs  \n m = idle (minimum) frequency, h = half frequence')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Workload ($\times 10E11$)')
plt.xticks(fontsize=8)

fig.autofmt_xdate()
plt.savefig("Google_pixel_workload_according_to_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()
################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy,workload), width=0.4)
# Add title and axis names
plt.title('Energy/ Workload according to the number of CPUs  \n  m = idle (minimum) frequency, h = half frequence')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Energy consumed/Workload ($\times 10E-11$)')
plt.xticks(fontsize=8)

fig.autofmt_xdate()
plt.savefig("Google_pixel_ratio_energy_by_workload_according_to_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()




######################################################## plotting graphs for the paper. 
# Showing that frequency influence the energy efficiency: ... Encore faut-il trouver la fréquence idéale. 
#  Dans l'expérimentation illustrée par la figure \ref{fig:frequencyInfluenceEnergyEfficiency}
#  Nous avons exécuté une tâche cpu intensive le BIG core du google Pixel 4A 5G
# A fréquence variable (sur l'axe des x), l'efficacité sur l'axe des Y, varie. 
# La fréquence idéale est celle du milieu c'est à dire 1.7 GHz .
# Ceci prouve que même pour les Big Core la fréquence maximale n'est pas toujours la plus efficace énergétiquement. 
number_of_cpus = ["mse",  "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",         "3m-0",  "3h-0"  , "3-0",        "1-1m" , "1-1h", "1m-1m", "1h-1h",   "1-1",                "0-1m",   "0-1h",  "0-1-0"  , "0-0-1",                                    "0-2m",  "0-2h" ,               "0-11m",  "0-11h",            "0-2"    ]                                                        
phone_energy= [ 1308, 11047.73,             22554.99,  24785.02,  29197.24,               24277.3,   28502.31 , 38598.14,        26060.77, 32111.43,  46077.78,     35087.51, 47102.84,  28158.18 , 42742.2,  73048.75,         30052.34 ,   47133.62, 66877.5,  77632.30 ,                            35683.20, 67647.63,            80676.10 ,  98665.02 ,                 115136.69   ]                                                                                                        
phone_power = [ 40.16, 336.07,               679.99,  746.01, 876.80,                      729.67,   854.14    , 1066.77,      783.20,  960.98,  1272.45,           1045.80, 1389.07,   845.63,   1265.01, 2106.08,            899.02,       1390.60, 1938.45,   2214.51  ,                         1062.76,  1958.13 ,          2298.82,   2758.98 ,                      3179.93   ]                                   
workload  = [ 0.1,         0.9,               0.6206,   1.267, 1.60362534,                 1.41737,  2.61014   , 3.368042,        2.291607, 4.07984,  4.9563,       3.810539, 6.249128,    2.790242, 6.12180,   8.47040,         2.5540681,    5.53843,  6.7442,   7.645522240   ,                   4.5668,   10.317,          9.392402,    12.280638,       13.21045181    ]          



#frequency = [806400, 1766400, 2400000 ]
frequency = ["0.81", "1.77", "2.4" ]                                                        
phone_energy= [ 30052.34, 47133.62, 77632.30 ]   
workload  = [ 2.5540681,  5.53843, 7.645522240 ]    
fontsize_in_paper = 20
bar_width = 0.4
label_format = '{:,.1f}'
fig, ((workload_fig, phone_energy_fig, energy_efficiency_fig)) = plt.subplots(nrows= 1, ncols = 3,  figsize=(15, 7), sharex=True)


#frequency[:] = [round(x / 1E+6, 2) for x in frequency]                                                                                                     
phone_energy[:] = [round(x / 1000, 2) for x in phone_energy]   #
energy_efficiency = np.divide(workload, phone_energy)    #*****# workload  (*1e11)   error = +- 0.0117 e 11 ;  phone energy is now in mAh , error =  +- 0.773  mAh
energy_efficiency_error =  [ 0.0117/e_ +  0.773*w_ / (e_ ** 2)  for w_, e_ in zip(workload, phone_energy)]  #*****# with error propagation, we have delta(energy_efficiency)/energy_efficiency = delta(workload)/workload + delta(energy)/energy
                           #*****#  so delta(energy_efficiency) = delta(worklaoad)/energy + delta(energy)*workload/energy^2
energy_efficiency[:] = [round(x*10, 2) for x in energy_efficiency]  #   energy efficiency has been multiplied by 10
energy_efficiency_error[:] = [x*10 for x in energy_efficiency_error]     #*****#  same thing for the error                                                                                                  
energy_efficiency_fig.bar(frequency,  energy_efficiency, yerr =energy_efficiency_error,  width=bar_width, color='gray')
energy_efficiency_fig.set_title('Energy efficiency\n ' + r'($\times 10e11$)', fontsize = fontsize_in_paper)
energy_efficiency_fig.set_xticklabels(frequency, fontsize = fontsize_in_paper)
energy_efficiency_fig.set_yticklabels([label_format.format(x) for x in energy_efficiency_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)

label_format = '{:,.0f}'

workload[:] = [round(x, 2 ) for x in workload]  
workload_error =  [0.0117] * len(frequency)     #*****#                                                                                          
workload_fig.bar(frequency, workload, yerr = workload_error, width= bar_width, color='gray')
workload_fig.set_title('Number of operations\n' + r'($\times 10e11$)', fontsize = fontsize_in_paper)
workload_fig.set_xticklabels(frequency, fontsize = fontsize_in_paper)
workload_fig.set_yticklabels([label_format.format(x) for x in workload_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)


#plt.xticks(fontsize=8)

phone_energy[:] = [round(x , 2 ) for x in phone_energy]  # I alrady divided by 1000        
energy_error =    [0.773] * len(frequency)     #*****#                                                                                             
phone_energy_fig.bar(frequency, phone_energy, yerr = energy_error, width=bar_width, color='gray')  # I divided the energy by 1000  because Moonson made a mistakes in the output, they gives energy in mAh 1000 times.
phone_energy_fig.set_title('Energy consumed\n' + r'(mAh)', fontsize = fontsize_in_paper)
phone_energy_fig.set_xticklabels(frequency, fontsize = fontsize_in_paper)
phone_energy_fig.set_yticklabels([label_format.format(x) for x in phone_energy_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)




#fig.suptitle("Workload, Energy and Energy efficiency according to the core frequency")
fig.supxlabel("Big core frequency (GHz)", fontsize = fontsize_in_paper)
#plt.gcf().autofmt_xdate()

plt.savefig("Frequency_influence_energy_efficiency.png")
plt.clf()
plt.cla()
plt.close()





######################################################## plotting graphs for the paper. 
# Showing that there are complex interaction between energy efficiency variables: 
#  Dans l'expérimentation illustrée par la figure \ref{fig:complexInteractionBetweenVariables}
#  Nous avons exécuté une tâche CPU intensive sur le google pixel 
# Nous avons d'une part fait variérié de 1 à 3 le nombre de threads parallélisé sur le little socket (1T to 3T). 
# D'autre part pour chaque nombre de threads nous avons fixé deux niveaux de fréquence différents , la fréquence moyenne (MidFreq = 1.36 GHz) et la fréquence maximale (MaxFreq = 1.8 GHz).  
# Nous pouvons par exemple noter que le nombre de threads et la fréquence interagissent entre elles. 
# Par exemple: 
#  Lorsque la fréquence est moyenne, augmenter le parallélisme de 1 à trois threads augmente l'efficacité de ratio 1
#   alors que lorsque la fréquence est élevée cette augmentation est de ratio 2
#  De plus lorsqu'un seul thread est démarrer, augmenter la fréquence du socket augmente l'efficacité énergétique. 
# Mais l'efficacité énergétique diminue lorsque 3 thread sont démarrés que que l'on effectue la même opération d'aumentation de fréquence. 
# Par ces  illustrations respectives nous voyons qu'il est crutial de considérer l'interaction entre les facteurs de l'efficacité énergétique, 
# C'est non seulement, pour augmenter exploiter les meilleures opportunités de gain mais aussi éviter des contres productivités.

number_of_cpus = ["mse",  "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",         "3m-0",  "3h-0"  , "3-0",        "1-1m" , "1-1h", "1m-1m", "1h-1h",   "1-1",                "0-1m",   "0-1h",  "0-1-0"  , "0-0-1",                                    "0-2m",  "0-2h" ,               "0-11m",  "0-11h",            "0-2"    ]                                                        
phone_energy= [ 1308, 11047.73,             22554.99,  24785.02,  29197.24,               24277.3,   28502.31 , 38598.14,        26060.77, 32111.43,  46077.78,     35087.51, 47102.84,  28158.18 , 42742.2,  73048.75,         30052.34 ,   47133.62, 66877.5,  77632.30 ,                            35683.20, 67647.63,            80676.10 ,  98665.02 ,                 115136.69   ]                                                                                                        
phone_power = [ 40.16, 336.07,               679.99,  746.01, 876.80,                      729.67,   854.14    , 1066.77,      783.20,  960.98,  1272.45,           1045.80, 1389.07,   845.63,   1265.01, 2106.08,            899.02,       1390.60, 1938.45,   2214.51  ,                         1062.76,  1958.13 ,          2298.82,   2758.98 ,                      3179.93   ]                                   
workload  = [ 0.1,         0.9,               0.6206,   1.267, 1.60362534,                 1.41737,  2.61014   , 3.368042,        2.291607, 4.07984,  4.9563,       3.810539, 6.249128,    2.790242, 6.12180,   8.47040,         2.5540681,    5.53843,  6.7442,   7.645522240   ,                   4.5668,   10.317,          9.392402,    12.280638,       13.21045181    ]          




frequency = ["1T,MidFreq", "1T,MaxFreq",   "2T,MidFreq", "2T,MaxFreq",  "3T,MidFreq", "3T,MaxFreq"  ]                                                        
phone_energy= [  24785.02,  29197.24,      28502.31 , 38598.14,                32111.43,  46077.78 ]   
workload  = [  1.267, 1.60362534,           2.61014   , 3.368042,          4.07984,  4.9563,  ]    
fontsize_in_paper = 20
bar_width = 0.4
label_format = '{:,.1f}'
fig, ((energy_efficiency_fig)) = plt.subplots(nrows= 1, ncols = 1,  figsize=(15, 7), sharex=True)


ratio_1 =     (workload[4]/phone_energy[4] -   workload[0]/phone_energy[0]) / (workload[0]/phone_energy[0])
ratio_2 =     (workload[5]/phone_energy[5] -   workload[0]/phone_energy[0]) / (workload[0]/phone_energy[0])

print(" ratio 1 ", ratio_1*100)
print(" ratio 2 ", ratio_2*100)

#frequency[:] = [round(x / 1E+6, 2) for x in frequency]                                                                                                     
phone_energy[:] = [round(x / 1000, 2) for x in phone_energy]   
energy_efficiency = np.divide(workload, phone_energy) #*****# workload  (*1e11)   error = +- 0.0117 e 11 ;  phone energy is now in mAh , error =  +- 0.773  mAh
energy_efficiency_error =  [ 0.0117/e_ +  0.773*w_ / (e_ ** 2)  for w_, e_ in zip(workload, phone_energy)]  #*****# with error propagation, we have delta(energy_efficiency)/energy_efficiency = delta(workload)/workload + delta(energy)/energy
                           #*****#  so delta(energy_efficiency) = delta(worklaoad)/energy + delta(energy)*workload/energy^2

energy_efficiency[:] = [round(x*10, 2) for x in energy_efficiency]      #   energy efficiency has been multiplied by 10
energy_efficiency_error[:] = [x*10 for x in energy_efficiency_error]     #*****#  same thing for the error                                                                                                   
energy_efficiency_fig.bar(frequency,  energy_efficiency, yerr = energy_efficiency_error , width=bar_width, color='gray')
energy_efficiency_fig.set_title('Energy efficiency\n ' + r'($\times 10e11$)', fontsize = fontsize_in_paper)
energy_efficiency_fig.set_xticklabels(frequency, fontsize = fontsize_in_paper)
energy_efficiency_fig.set_yticklabels([label_format.format(x) for x in energy_efficiency_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)

"""
label_format = '{:,.0f}'

workload[:] = [round(x, 2 ) for x in workload]                                                                                                     
workload_fig.bar(frequency, workload, width= bar_width, color = "black")
workload_fig.set_title('Number of operations\n' + r'($\times 10e11$)', fontsize = fontsize_in_paper)
workload_fig.set_xticklabels(frequency, fontsize = fontsize_in_paper)
workload_fig.set_yticklabels([label_format.format(x) for x in workload_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)


#plt.xticks(fontsize=8)

phone_energy[:] = [round(x , 2 ) for x in phone_energy]  # I alrady divided by 1000                                                                                                   
phone_energy_fig.bar(frequency, phone_energy, width=bar_width, color = "black")  # I divided the energy by 1000  because Moonson made a mistakes in the output, they gives energy in mAh 1000 times.
phone_energy_fig.set_title('Energy consumed\n' + r'(mAh)', fontsize = fontsize_in_paper)
phone_energy_fig.set_xticklabels(frequency, fontsize = fontsize_in_paper)
phone_energy_fig.set_yticklabels([label_format.format(x) for x in phone_energy_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)
"""



#fig.suptitle("Workload, Energy and Energy efficiency according to the core frequency")
fig.supxlabel("Big core frequency (GHz)", fontsize = fontsize_in_paper)
plt.gcf().autofmt_xdate()

plt.savefig("Complexity_Between_Vars_influence_energy_efficiency.png")
plt.clf()
plt.cla()
plt.close()

