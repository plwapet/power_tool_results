from ctypes import sizeof
import matplotlib.pyplot as plt
import numpy as np
import math
import os





"""
configurations = ["mse",  "idl" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",         "3m-0",  "3h-0"  , "3-0",        "1-1m" , "1-1h", "1m-1m", "1h-1h",  "1-1",                   "0-1m",   "0-1h",  "0-1-0",     , "0-0-1",                                    "0-2m",  "0-2h" ,               "0-11m",  "0-11h",            "0-2"    ]                                                        
phone_energy= [ 1308, 11047.73,             22554.99,  24785.02,  29197.24,               24277.3,   28502.31 , 38598.14,        26060.77, 32111.43,  46077.78,     35087.51, 47102.84,  28158.18 , 42742.2, 73048.75,         30052.34 ,47133.62,  66877.5  , 77632.30 ,                            35683.20, 67647.63,            80676.10 ,  98665.02 ,                 115136.69   ]                                                                                                        
phone_power = [ 40.16, 336.07,               679.99,  746.01, 876.80,                      729.67,   854.14    , 1066.77,      783.20,  960.98,  1272.45,           1045.80, 1389.07,   845.63,   1265.01, 2106.08,          899.02,       1390.60, 1938.45  , 2214.51  ,                         1062.76,  1958.13 ,          2298.82,   2758.98 ,                      3179.93   ]                                   
workload  = [ 0.1,         0.9,               0.6206,   1.267, 1.60362534,                 1.41737,  2.61014   , 3.368042,        2.291607, 4.07984,  4.9563,       3.810539, 6.249128,    2.790242, 6.12180,  8.47040,        2.5540681,    5.53843,  6.7442,  7.645522240   ,                   4.5668,   10.317,          9.392402,    12.280638,       13.21045181    ]          
"""

"""

configurations = ["mse",  "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",         "3m-0",  "3h-0"  , "3-0",        "1-1m" , "1-1h", "1m-1m", "1h-1h",   "1-1",                       "0-1m",   "0-1h", "0-1-0", "0-0-1",                                    "0-2m",  "0-2h" ,               "0-11m",  "0-11h",            "0-2"    ]                                                        
phone_energy= [ 1308, 11047.73,             22554.99,  24785.02,  29197.24,               24277.3,   28502.31 , 38598.14,        26060.77, 32111.43,  46077.78,     35087.51, 47102.84,  28158.18 , 42742.2,  73048.75,         30052.34 ,    47133.62,   66877.5 ,77632.30 ,                            35683.20, 67647.63,            80676.10 ,  98665.02 ,                 115136.69   ]                                                                                                        
phone_power = [ 40.16, 336.07,               679.99,  746.01, 876.80,                      729.67,   854.14    , 1066.77,      783.20,  960.98,  1272.45,           1045.80, 1389.07,   845.63,   1265.01, 2106.08,            899.02,       1390.60,    1938.45 ,  2214.51  ,                         1062.76,  1958.13 ,          2298.82,   2758.98 ,                      3179.93   ]                                   
workload  = [ 0.1,         0.9,               0.6206,   1.267, 1.60362534,                 1.41737,  2.61014   , 3.368042,        2.291607, 4.07984,  4.9563,       3.810539, 6.249128,    2.790242, 6.12180,   8.47040,         2.5540681,    5.53843,    6.7442, 7.645522240   ,                   4.5668,   10.317,          9.392402,    12.280638,       13.21045181    ]          
"""
# I used the final datas without the mouse résult
configurations = [ "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",         "3m-0",  "3h-0"  , "3-0",        "1-1m" , "1-1h", "1m-1m", "1h-1h",   "1-1",             "0-1m",   "0-1h",      "0-1-0",    "0-0-1",                                    "0-2m",  "0-2h" ,               "0-11m",  "0-11h",            "0-2"    ]                                                        
phone_energy= [ 11047.73,        22554.99,  24785.02,  29197.24,               24277.3,   28502.31 , 38598.14,        26060.77, 32111.43,  46077.78,     35087.51, 47102.84,  28158.18 , 42742.2,  73048.75,         30052.34 ,    47133.62,  66877.5 , 77632.30 ,                            35683.20, 67647.63,            80676.10 ,  98665.02 ,                 115136.69   ]                                                                                                        
phone_power = [  336.07,           679.99,  746.01, 876.80,                      729.67,   854.14    , 1066.77,      783.20,  960.98,  1272.45,           1045.80, 1389.07,   845.63,   1265.01, 2106.08,            899.02,       1390.60,   1938.45 ,   2214.51  ,                         1062.76,  1958.13 ,          2298.82,   2758.98 ,                      3179.93   ]                                   
workload  = [    0.9,               0.6206,   1.267, 1.60362534,                 1.41737,  2.61014   , 3.368042,        2.291607, 4.07984,  4.9563,       3.810539, 6.249128,    2.790242, 6.12180,   8.47040,         2.5540681,    5.53843,   6.7442,   7.645522240   ,                   4.5668,   10.317,          9.392402,    12.280638,       13.21045181    ]          
workload = np.multiply(workload,10**11)  # Note !!! In previous machine learning models I did not realize this multiplication. 
                                         # It is mandatory to restart generation process if I need to compare with automatic experiment, in which I printed the exact value of workload. 
phone_energy=np.divide(phone_energy,10**3)


cwd = os.getcwd()
print ("current directory = ")
print(cwd)
dir = os.path.join(cwd,"machine_learning_datas")
if not os.path.exists(dir):
    os.mkdir(dir)

with open('machine_learning_datas/configurations_user_friendly.txt','w') as file:
    print("--- writing the phone configurations in user friendly format")
    counter = 1
    for value in configurations:
            file.write(str(value))
            if (counter < len(configurations)):
                file.write('\n')
                counter = counter + 1



with open('machine_learning_datas/configurations_generic_format.txt','w') as file:
    print("--- writing the configurations for generic phone in format (l,l,l,l,l,l,m,m,m,b,b,b,b)")
    #for generic phone, we have 6 maximum little cores, 3 maximum medium cores and 4 maximum big cores
    # for the frequency we consider that 1 means minimum or idle, 2 means half frequency, and 3 means maximum frequency
    # On this datas we vary the number of thread, for this reason all value are 2, supposing that cpu running the thread are on maximum frequency. 
    # For now we don't add equivalent values, we will add them later if possible, but we consider that on the same socket the core on which the thread is pinned does not matter. 
    # for exemple  (3,0,0,0,0,0, 0,0,0, 0,0,0,0), # could be equivalent to  (0,3,0,0,0,0, 0,0,0, 0,0,0,0)
    configurations = [ "idl" ,       
    "1m-0", "1h-0", "1-0",                                
    "2m-0",  "2h-0"  , "2-0",         
    "3m-0",  "3h-0"  , "3-0",        
    "1-1m-0" , "1-1h-1", "1m-1m-0", "1h-1h-0",   "1-0-1",
    "0-0-1m",   "0-0-1h", "0-1-0", "0-0-1",                                    
   
    "0-1m-1m",  "0-1h-1h" ,               
    "0-3-1m",  "0-3-1h",            
    "0-1-1"    ]                                                        

    file.write("0,0,0,0,0,0, 0,0,0, 0,0,0,0\n" 
        "1,0,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "2,0,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "1,1,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "2,2,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "1,1,1,0,0,0, 0,0,0, 0,0,0,0\n"
        "2,2,2,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,3,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,0,0,0,0,0, 1,0,0, 0,0,0,0\n"##
        "3,0,0,0,0,0, 2,0,0, 0,0,0,0\n"##
        "1,0,0,0,0,0, 1,0,0, 0,0,0,0\n"##
        "2,0,0,0,0,0, 2,0,0, 0,0,0,0\n"##
        "3,0,0,0,0,0, 0,0,0, 3,0,0,0\n"##
        "0,0,0,0,0,0, 0,0,0, 1,0,0,0\n"##
        "0,0,0,0,0,0, 0,0,0, 2,0,0,0\n"##
        "0,0,0,0,0,0, 3,0,0, 0,0,0,0\n"##
        "0,0,0,0,0,0, 0,0,0, 3,0,0,0\n"##

        "0,0,0,0,0,0, 1,0,0, 1,0,0,0\n"##
        "0,0,0,0,0,0, 2,0,0, 2,0,0,0\n"##
        "0,0,0,0,0,0, 3,0,0, 1,0,0,0\n"##
        "0,0,0,0,0,0, 3,0,0, 2,0,0,0\n"##
        "0,0,0,0,0,0, 0,0,0, 3,3,0,0\n"##
        )

with open('machine_learning_datas/configurations_google_pixel_format.txt','w') as file:
    print("--- writing the configuration for google pixel in format (l,l,l,l,l,l,m,b)")
    #  we have 6 maximum little cores, 1 medium core and 1  big core"
    # we also have same considerations as above with the generic configuration
    file.write("0,0,0,0,0,0, 0,0\n" 
         "1,0,0,0,0,0, 0,0\n"
        "2,0,0,0,0,0,0,0\n"
        "3,0,0,0,0,0,0,0\n"
        "1,1,0,0,0,0,0,0\n"
        "2,2,0,0,0,0,0,0\n"
        "3,3,0,0,0,0,0,0\n"
        "1,1,1,0,0,0,0,0\n"
        "2,2,2,0,0,0,0,0\n"
        "3,3,3,0,0,0,0,0\n"
        "3,0,0,0,0,0,1,0\n"
        "3,0,0,0,0,0,2,0\n"
        "1,0,0,0,0,0,1,0\n"
        "2,0,0,0,0,0,2,0\n"
        "3,0,0,0,0,0,3,0\n"
        "0,0,0,0,0,0,1,0\n"
        "0,0,0,0,0,0,2,0\n"
        "0,0,0,0,0,0,3,0\n"
        "0,0,0,0,0,0,0,3\n"
        "0,0,0,0,0,0,1,1\n"
        "0,0,0,0,0,0,2,2\n"
        "0,0,0,0,0,0,3,1\n"
        "0,0,0,0,0,0,3,2\n"
        "0,0,0,0,0,0,3,3\n")

with open('machine_learning_datas/phone_energy.txt','w') as file:
    print("--- writing the phone energy")
    counter = 1
    for value in phone_energy:
            file.write(str(value))
            if (counter < len(phone_energy)):
                file.write('\n')
                counter = counter + 1

with open('machine_learning_datas/phone_power.txt','w') as file:
    print("--- writing the phone power")
    counter = 1
    for value in phone_power:
        file.write(str(value))
        if (counter < len(phone_energy)):
            file.write('\n')
            counter = counter + 1

with open('machine_learning_datas/workload_computed.txt','w') as file:
    print("--- writing the workload comuted")
    counter = 1
    for value in workload:
        file.write(str(value))
        if (counter < len(workload)):
            file.write('\n')
            counter = counter + 1
    

with open('machine_learning_datas/ratio_energy_by_worload.txt','w') as file:
    print("--- writing the ratio energy by workload")
    ratio = np.divide(phone_energy,workload)
    counter = 1
    for value in ratio:
        file.write(str(value))
        if (counter < len(ratio)):
            file.write('\n')
            counter = counter + 1
  






#### Taking into account the real value of frequency 
# for a particular dictionnary this function transform the observation X[i], to reflect exact values of frequency
# possible dictionnaries can be: 
#  1 - dictionnary of experiment on google pixel 4a 5g on little socket, 
#          0 ->  0 
#          1 -> 576000   (it is the minimum usable frequency on little core on google pixel 4a 5g)
#          2 -> 1363200  (it is the mid frequency on little core on google pixel 4a 5g)
#          3 -> 1804800  (it is the max frequency on little core on google pixel 4a 5g)
freq_dict_of_little_socket_google_pixel = { 0: 0, 1: 576000,  2: 1363200,  3: 1804800  }
#  2 - dictionnary of experiments on google pixel 4a 5g  on medium socket, 
#          0 -> 0
#          1 -> 652800   (it is the minimum usable frequency on big core on google pixel 4a 5g)
#          2 -> 1478400  (it is the mid frequency on big core on google pixel 4a 5g)
#          3 -> 2208000  (it is the max frequency on big core on google pixel 4a 5g )
freq_dict_of_medium_socket_google_pixel = { 0: 0, 1: 652800,  2: 1478400,  3: 2208000  }
#### Before continue I put there a big warning because, I need to reconsider the fact that google pixel à three types of cores (medium, little, big cores)
####
#  3 - dictionnary of experiment on google pixel 4a 5g on big socket, 
#          0 ->  0 
#          1 -> 806400   (it is the minimum usable frequency on big core on google pixel 4a 5g)
#          2 -> 1766400   (it is the mid frequency on big core on google pixel 4a 5g)
#          3 -> 2400000  (it is the max frequency on big core on google pixel 4a 5g )
freq_dict_of_big_socket_google_pixel = { 0: 0, 1: 806400,  2: 1766400,  3: 2400000  }

def trans_form_X_generic_to_have_real_values_of_frequency(X, freq_dict_of_little_socket , freq_dict_of_medium_socket, freq_dict_of_big_socket):
    result = []
    for x in X:
        temp_x = [] 
        for i in range(0,6):
            temp_x.append(freq_dict_of_little_socket[x[i]])
        for i in range(6,9):
            temp_x.append(freq_dict_of_medium_socket[x[i]])
        for i in range(9,13):
            temp_x.append(freq_dict_of_big_socket[x[i]])
        result.append(temp_x)
    return result

def trans_form_X_google_format_to_have_real_values_of_frequency(X, freq_dict_of_little_socket , freq_dict_of_medium_socket, freq_dict_of_big_socket):
    result = []
    for x in X:
        temp_x = [] 
        for i in range(0,6):
            temp_x.append(freq_dict_of_little_socket[x[i]])
        temp_x.append(freq_dict_of_medium_socket[x[6]])
        temp_x.append(freq_dict_of_big_socket[x[7]])
        result.append(temp_x)
    return result

def read_configuration_X(file_path):
    print("---> Reading the data set file X in generic format") 
    X = []
    with open(file_path) as fp:
        line = fp.readline()
        cnt = 1
        while line:
            #print("reading line {}: {}".format(cnt, line.strip()))
            str_X_line = line.split(',')
            #print ("arrays as string : ", str_X_line)
            X_line = [int(numeric_string) for numeric_string in str_X_line]
            #print("resulted array : ", X_line)
            X.append(X_line)
            #print("after adding it to X : ", X)
            line = fp.readline()
            cnt += 1
    return X
# now writing configurations in generic format with exact value of frequency
with open('machine_learning_datas/configurations_generic_format_exact_frequency_values.txt','w') as file:
    print("--- writing the configurations with exact values of frequency in generic format")
    _google_pixel_4a_5g_varying_frequency_generic = read_configuration_X("machine_learning_datas/configurations_generic_format.txt")
    _google_pixel_4a_5g_varying_frequency_generic_exact_freq = trans_form_X_generic_to_have_real_values_of_frequency(
         _google_pixel_4a_5g_varying_frequency_generic,
         freq_dict_of_little_socket_google_pixel,
         freq_dict_of_medium_socket_google_pixel,
         freq_dict_of_big_socket_google_pixel
    )
    counter_line = 1
    line = []
    for line in _google_pixel_4a_5g_varying_frequency_generic_exact_freq:
        counter_value = 1
        for value in line:
            file.write(str(value))
            if (counter_value < len(line)):
                file.write(',')
                counter_value = counter_value + 1      
        if (counter_line < len(_google_pixel_4a_5g_varying_frequency_generic_exact_freq)):
            file.write('\n')
            counter_line = counter_line + 1
  

# now writing configurations in google pixel format with exact value of frequency
with open('machine_learning_datas/configurations_google_pixel_format_exact_frequency_values.txt','w') as file:
    print("--- writing the configurations with exact values of frequency in google format")
    _google_pixel_4a_5g_varying_frequency_google_pixel_format = read_configuration_X("machine_learning_datas/configurations_google_pixel_format.txt")
    _google_pixel_4a_5g_varying_frequency_google_pixel_format_exact_freq = trans_form_X_google_format_to_have_real_values_of_frequency(
         _google_pixel_4a_5g_varying_frequency_google_pixel_format,
         freq_dict_of_little_socket_google_pixel,
         freq_dict_of_medium_socket_google_pixel,
         freq_dict_of_big_socket_google_pixel
    )
    counter_line = 1
    line = []
    for line in _google_pixel_4a_5g_varying_frequency_google_pixel_format_exact_freq:
        counter_value = 1
        for value in line:
            file.write(str(value))
            if (counter_value < len(line)):
                file.write(',')
                counter_value = counter_value + 1      
        if (counter_line < len(_google_pixel_4a_5g_varying_frequency_google_pixel_format_exact_freq)):
            file.write('\n')
            counter_line = counter_line + 1
  

# now writing all valuable datas on csv format

data_transposed = []
data_transposed.append(configurations)
data_transposed.append(_google_pixel_4a_5g_varying_frequency_generic)
data_transposed.append(_google_pixel_4a_5g_varying_frequency_generic_exact_freq)
data_transposed.append(_google_pixel_4a_5g_varying_frequency_google_pixel_format)
data_transposed.append(_google_pixel_4a_5g_varying_frequency_google_pixel_format_exact_freq)
data_transposed.append(phone_energy)
data_transposed.append(phone_power)
data_transposed.append(workload)
data_transposed.append(list(np.divide(phone_energy,workload)))
print( data_transposed)
data = (np.array(data_transposed, dtype=object)).T

header = ["configurations", "generic format", "exact frequency", 
"google pixel format", "exact frequencies", 
"phone energy", "phone power", "workload", "energy by workload"]
print ("final data", data)

with open('machine_learning_datas/summary.csv','w') as file:
    counter = 1
    for title in header:
        file.write(title)
        if (counter < len(header)):
            file.write(',')
            counter = counter + 1
    file.write('\n')
    
    line = []
    for line in data:
        counter_value = 1
        for value in line:
            file.write(str(value).replace(",", "-"))
            if (counter_value < len(line)):
                file.write(',')
                counter_value = counter_value + 1
        file.write('\n')
#data.tofile('summary.txt', sep = ',')
#np.savetxt('summary.csv', data, delimiter=',') 













# for machine learning process I did not use the "mouse" results

"""
txt = ["PS5", "is", "currently", "unavailable", "in", "India"]

with open('shows.csv','w') as file:
    for line in txt:
        file.write(line)
        file.write('\n')

plt.bar(configurations,phone_power, width, label='With new usb cable')


plt.bar(configurations,phone_energy, width=0.4)

#################################################


plt.bar(configurations,np.divide(workload,100000000000), width=0.4)


plt.bar(configurations,np.divide(phone_energy,workload), width=0.4)

"""

