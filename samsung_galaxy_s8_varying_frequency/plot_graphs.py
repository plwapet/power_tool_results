import matplotlib.pyplot as plt
import numpy as np
import math



           #-->12: (2 little, 1 big)             //pour voir si les littles mènent la danse



plt.rcParams.update({'mathtext.default':  'regular' })

""""
original datas
                                                                 # datas of 1-0, 2-0 , 0-1, 0-2 comes from previous experiments,                                                            to add there "1-1m" , "1-1h",
number_of_cpus = ["mse",  "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",                "3m-0",  "3h-0"  , "3-0",                      "1-1m" , "1-1h",        "1m-1m", "1h-1h",   "1-1",             "0-1m",   "0-1h", "0-1",                        "0-2m",  "0-2h" , "0-2"  ]                                                                   
phone_energy= [ 1308,  15437.75 ,      37670  ,  48679.35, 51208.79,                           44536.9, 48031.53  ,54347.47,            42579.4 , 46728.37,    57513.95,               56196.02, 67856.14,   47490.44,  58233.75,  90216.11,        42509.65,  53511.44, 94219.85,                    56508.73, 77029.61 ,  100720.96  ]                                                                                                        
phone_power = [ 40.16, 1047.42   ,      2622.13 ,  2617.80, 2617.20,                            2140.92,   2141.83  , 2532.40,             1915.19, 1737.81, 1950.01,                   2581.74, 2359.28 ,    2612.70,  938.61,   2620.50,          2618.50 ,  2619.78, 2620.1 ,                       2613.45, 2613.11 ,  2622.43   ]                                   
workload  = [ 0.1,         0.9,       0.453494357, 0.89093211,1.25068,                        0.974057264, 1.94124254  ,  2.688753,         1.570584138, 2.83975072, 4.02600,         2.447583520,  3.742789,  1.5484795,  3.3033915,   4.550717,   1.20441511,  2.471412861,  3.619594,             2.397140, 5.094311 , 6.09298   ]              

"""

""" data without some of  experiments  on which both sockets are occupied
                                                                 # datas of 1-0, 2-0 , 0-1, 0-2 comes from previous experiments,                                        to add there "1-1m" , "1-1h",
number_of_cpus = ["mse",  "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",                "3m-0",  "3h-0"  , "3-0",                      "1-1m" , "1-1h",        "1m-1m", "1h-1h",   "1-1",             "0-1m",   "0-1h", "0-1",                        "0-2m",  "0-2h" , "0-2"  ]                                                                   
phone_energy= [ 1308,  15437.75 ,      37670  ,  48679.35, 51208.79,                           44536.9, 48031.53  ,54347.47,            42579.4 , 46728.37,    57513.95,               56196.02, 67856.14,   47490.44,  58233.75,  90216.11,        42509.65,  53511.44, 94219.85,                    56508.73, 77029.61 ,  100720.96  ]                                                                                                        
phone_power = [ 40.16, 1047.42   ,      2622.13 ,  2617.80, 2617.20,                            2140.92,   2141.83  , 2532.40,             1915.19, 1737.81, 1950.01,                   2581.74, 2359.28 ,    2612.70,  938.61,   2620.50,          2618.50 ,  2619.78, 2620.1 ,                       2613.45, 2613.11 ,  2622.43   ]                                   
workload  = [ 0.1,         0.9,       0.453494357, 0.89093211,1.25068,                        0.974057264, 1.94124254  ,  2.688753,         1.570584138, 2.83975072, 4.02600,         2.447583520,  3.742789,  1.5484795,  3.3033915,   4.550717,   1.20441511,  2.471412861,  3.619594,             2.397140, 5.094311 , 6.09298   ]              
"""

"""
# data to see configurations on which both sockets are occupied
number_of_cpus = [  "2-1m" ,  "2-1h",   "2m-1m",  "2h-1h", "2-1",           |  "1-1m" , "1-1h",        "1m-1m", "1h-1h",   "1-1",         |    "1-2m", "1-2h",              "1m-2m", "1h-2h", "1-2"  ]                                                                   
phone_energy= [      60464.2, 71507.54,  42530.26,  60764.34 ,98711.42,      | 56196.02, 67856.14,   47490.44,  58233.75,  90216.11,      |   63382.73,   85482.98,          48185.71,  75440.60,  110737]                                                                                                        
phone_power = [     1879.23,  2034.51,   1258.93,  2022.83 ,    2619.02,      |  2581.74, 2359.28 ,    2612.70,  938.61,   2620.50,        |   1838.65, 2390.40,              1419.65,   2166.68,   2621.93]                                   
workload  = [     3.806468390, 5.180210,  2.007837, 4.3029538,    6.14819,   | 2.447583520,  3.742789,  1.5484795,  3.3033915,   4.550717,  |  3.746194, 6.45796,           2.9628122,  6.14480243647,  7.632924365 ]              
"""

#data with some of  experiments  on which both sockets are occupied
                                                                 # datas of 1-0, 2-0 , 0-1, 0-2 comes from previous experiments,                                        to add there "1-1m" , "1-1h", 
number_of_cpus = ["mse",  "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",                "3m-0",  "3h-0"  , "3-0",                     "2-1m" ,  "2-1h",   "2m-1m",     "2h-1h", "2-1",                 "1-1m" , "1-1h",               "1m-1m", "1h-1h",   "1-1",               "1-2m", "1-2h",              "1m-2m", "1h-2h", "1-2" ,                               "0-1m",   "0-1h", "0-1",                        "0-2m",  "0-2h" , "0-2"  ]                                                                   
phone_energy= [ 1308,  15437.75 ,      37670  ,  48679.35, 51208.79,                           44536.9, 48031.53  ,54347.47,            42579.4 , 46728.37,    57513.95,           60464.2, 71507.54,  42530.26,       60764.34 ,98711.42,           56196.02, 67856.14,             47490.44,  58233.75,  90216.11,        63382.73,   85482.98,          48185.71,  75440.60,  110737,                     42509.65,  53511.44, 94219.85,                    56508.73, 77029.61 ,  100720.96  ]                                                                                                        
phone_power = [ 40.16, 1047.42,        2622.13 ,  2617.80, 2617.20,                         2140.92,   2141.83, 2532.40,               1915.19, 1737.81, 1950.01,               1879.23,  2034.51,   1258.93,         2022.83,  2619.02,             2581.74, 2359.28 ,                  2612.70,  938.61,   2620.50,              1838.65, 2390.40,       1419.65,   2166.68,   2621.93,                        2618.50 ,  2619.78, 2620.1 ,                       2613.45, 2613.11 ,  2622.43   ]                                   
workload  = [ 0.1,         0.9,       0.453494357, 0.89093211,1.25068,                   0.974057264, 1.94124254  ,  2.688753,        1.570584138, 2.83975072, 4.02600,         3.806468390, 5.180210,  2.007837,     4.3029538, 6.14819,        2.447583520,  3.742789,     1.5484795,  3.3033915,   4.550717,             3.746194, 6.45796,      2.9628122,  6.14480243647,  7.632924365,          1.20441511,  2.471412861,  3.619594,             2.397140, 5.094311 , 6.09298   ]              







fig = plt.figure(figsize=(9, 5))
plt.bar(number_of_cpus,phone_power, width=0.4)
 
# Add title and axis names
plt.title('Avg power used by the phone according to the thread configuration \n m = idle (minimum) frequency, h = half frequence ')
plt.xlabel('Number of CPUs')
plt.ylabel('AVG Power')
plt.xticks(fontsize=8)


fig.autofmt_xdate()
plt.savefig("samsung_galaxy_s8_power_absorbed_according_to_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()


fig = plt.figure()
fig = plt.figure(figsize=(9, 5))
plt.bar(number_of_cpus,phone_energy, width=0.4)
 
# Add title and axis names
plt.title('Energy consumed by the phone according to the thread configuration \n  m = idle (minimum) frequency, h = half frequence')
plt.xlabel('Number of CPUs')
plt.ylabel('Energy consumed')
plt.xticks(fontsize=8)

fig.autofmt_xdate()
plt.savefig("samsung_galaxy_s8_energy_consumed_according_to_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()

#################################################

fig = plt.figure()
fig = plt.figure(figsize=(9, 5))
plt.bar(number_of_cpus,np.divide(workload,100000000000), width=0.4)

# Add title and axis names
plt.title('New computed Workload according to the number of CPUs  \n m = idle (minimum) frequency, h = half frequence')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Workload ($\times 10E11$)')
plt.xticks(fontsize=8)

fig.autofmt_xdate()
plt.savefig("samsung_galaxy_s8_workload_according_to_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()
################################################

fig = plt.figure()
fig = plt.figure(figsize=(9, 5))
plt.bar(number_of_cpus,np.divide(phone_energy,workload), width=0.4)
# Add title and axis names
plt.title('Energy/ Workload according to the number of CPUs  \n  m = idle (minimum) frequency, h = half frequence')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Energy consumed/Workload ($\times 10E-11$)')
plt.xticks(fontsize=8)

fig.autofmt_xdate()
plt.savefig("samsung_galaxy_s8_ratio_energy_by_workload_according_to_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()

