from ctypes import sizeof
import matplotlib.pyplot as plt
import numpy as np
import math
import os






""""
original datas
                                                                 # datas of 1-0, 2-0 , 0-1, 0-2 comes from previous experiments,                                                            to add there "1-1m" , "1-1h",
configurations = ["mse",  "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",                "3m-0",  "3h-0"  , "3-0",                      "1-1m" , "1-1h",        "1m-1m", "1h-1h",   "1-1",             "0-1m",   "0-1h", "0-1",                        "0-2m",  "0-2h" , "0-2"  ]                                                                   
phone_energy= [ 1308,  15437.75 ,      37670  ,  48679.35, 51208.79,                           44536.9, 48031.53  ,54347.47,            42579.4 , 46728.37,    57513.95,               56196.02, 67856.14,   47490.44,  58233.75,  90216.11,        42509.65,  53511.44, 94219.85,                    56508.73, 77029.61 ,  100720.96  ]                                                                                                        
phone_power = [ 40.16, 1047.42   ,      2622.13 ,  2617.80, 2617.20,                            2140.92,   2141.83  , 2532.40,             1915.19, 1737.81, 1950.01,                   2581.74, 2359.28 ,    2612.70,  938.61,   2620.50,          2618.50 ,  2619.78, 2620.1 ,                       2613.45, 2613.11 ,  2622.43   ]                                   
workload  = [ 0.1,         0.9,       0.453494357, 0.89093211,1.25068,                        0.974057264, 1.94124254  ,  2.688753,         1.570584138, 2.83975072, 4.02600,         2.447583520,  3.742789,  1.5484795,  3.3033915,   4.550717,   1.20441511,  2.471412861,  3.619594,             2.397140, 5.094311 , 6.09298   ]              

"""

""" data without some of  experiments  on which both sockets are occupied
                                                                 # datas of 1-0, 2-0 , 0-1, 0-2 comes from previous experiments,                                        to add there "1-1m" , "1-1h",
configurations = ["mse",  "idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",                "3m-0",  "3h-0"  , "3-0",                      "1-1m" , "1-1h",        "1m-1m", "1h-1h",   "1-1",             "0-1m",   "0-1h", "0-1",                        "0-2m",  "0-2h" , "0-2"  ]                                                                   
phone_energy= [ 1308,  15437.75 ,      37670  ,  48679.35, 51208.79,                           44536.9, 48031.53  ,54347.47,            42579.4 , 46728.37,    57513.95,               56196.02, 67856.14,   47490.44,  58233.75,  90216.11,        42509.65,  53511.44, 94219.85,                    56508.73, 77029.61 ,  100720.96  ]                                                                                                        
phone_power = [ 40.16, 1047.42   ,      2622.13 ,  2617.80, 2617.20,                            2140.92,   2141.83  , 2532.40,             1915.19, 1737.81, 1950.01,                   2581.74, 2359.28 ,    2612.70,  938.61,   2620.50,          2618.50 ,  2619.78, 2620.1 ,                       2613.45, 2613.11 ,  2622.43   ]                                   
workload  = [ 0.1,         0.9,       0.453494357, 0.89093211,1.25068,                        0.974057264, 1.94124254  ,  2.688753,         1.570584138, 2.83975072, 4.02600,         2.447583520,  3.742789,  1.5484795,  3.3033915,   4.550717,   1.20441511,  2.471412861,  3.619594,             2.397140, 5.094311 , 6.09298   ]              
"""

"""
# data to see configurations on which both sockets are occupied
configurations = [  "2-1m" ,  "2-1h",   "2m-1m",  "2h-1h", "2-1",           |  "1-1m" , "1-1h",        "1m-1m", "1h-1h",   "1-1",         |    "1-2m", "1-2h",              "1m-2m", "1h-2h", "1-2"  ]                                                                   
phone_energy= [      60464.2, 71507.54,  42530.26,  60764.34 ,98711.42,      | 56196.02, 67856.14,   47490.44,  58233.75,  90216.11,      |   63382.73,   85482.98,          48185.71,  75440.60,  110737]                                                                                                        
phone_power = [     1879.23,  2034.51,   1258.93,  2022.83 ,    2619.02,      |  2581.74, 2359.28 ,    2612.70,  938.61,   2620.50,        |   1838.65, 2390.40,              1419.65,   2166.68,   2621.93]                                   
workload  = [     3.806468390, 5.180210,  2.007837, 4.3029538,    6.14819,   | 2.447583520,  3.742789,  1.5484795,  3.3033915,   4.550717,  |  3.746194, 6.45796,           2.9628122,  6.14480243647,  7.632924365 ]              
"""

#data with some of  experiments  on which both sockets are occupied
                                                                 # datas of 1-0, 2-0 , 0-1, 0-2 comes from previous experiments,                                        to add there "1-1m" , "1-1h", 





# I used the final datas without the mouse résult
configurations = ["idle" ,       "1m-0", "1h-0", "1-0",                                "2m-0",  "2h-0"  , "2-0",                "3m-0",  "3h-0"  , "3-0",                     "2-1m" ,  "2-1h",   "2m-1m",     "2h-1h", "2-1",                 "1-1m" , "1-1h",               "1m-1m", "1h-1h",   "1-1",               "1-2m", "1-2h",              "1m-2m", "1h-2h", "1-2" ,                               "0-1m",   "0-1h", "0-1",                        "0-2m",  "0-2h" , "0-2"  ]                                                                   
phone_energy= [ 15437.75 ,      37670  ,  48679.35, 51208.79,                           44536.9, 48031.53  ,54347.47,            42579.4 , 46728.37,    57513.95,           60464.2, 71507.54,  42530.26,       60764.34 ,98711.42,           56196.02, 67856.14,             47490.44,  58233.75,  90216.11,        63382.73,   85482.98,          48185.71,  75440.60,  110737,                     42509.65,  53511.44, 94219.85,                    56508.73, 77029.61 ,  100720.96  ]                                                                                                        
phone_power = [ 1047.42,        2622.13 ,  2617.80, 2617.20,                         2140.92,   2141.83, 2532.40,               1915.19, 1737.81, 1950.01,               1879.23,  2034.51,   1258.93,         2022.83,  2619.02,             2581.74, 2359.28 ,                  2612.70,  938.61,   2620.50,              1838.65, 2390.40,       1419.65,   2166.68,   2621.93,                        2618.50 ,  2619.78, 2620.1 ,                       2613.45, 2613.11 ,  2622.43   ]                                   
workload  = [    0.9,       0.453494357, 0.89093211,1.25068,                   0.974057264, 1.94124254  ,  2.688753,        1.570584138, 2.83975072, 4.02600,         3.806468390, 5.180210,  2.007837,     4.3029538, 6.14819,        2.447583520,  3.742789,     1.5484795,  3.3033915,   4.550717,             3.746194, 6.45796,      2.9628122,  6.14480243647,  7.632924365,          1.20441511,  2.471412861,  3.619594,             2.397140, 5.094311 , 6.09298   ]              
workload = np.multiply(workload,10**11)  # Note !!! In previous machine learning models I did not realize this multiplication. 
                                         # It is mandatory to restart generation process if I need to compare with automatic experiment, in which I printed the exact value of workload. 
phone_energy=np.divide(phone_energy,10**3)   


cwd = os.getcwd()
print ("current directory = ")
print(cwd)
dir = os.path.join(cwd,"machine_learning_datas")
if not os.path.exists(dir):
    os.mkdir(dir)

with open('machine_learning_datas/configurations_user_friendly.txt','w') as file:
    print("--- writing the phone configurations in user friendly format")
    counter = 1
    for value in configurations:
            file.write(str(value))
            if (counter < len(configurations)):
                file.write('\n')
                counter = counter + 1



with open('machine_learning_datas/configurations_generic_format.txt','w') as file:
    print("--- writing the configurations for generic phone in format (l,l,l,l,l,l,m,m,m,b,b,b,b)")
    #for generic phone, we have 6 maximum little cores, 3 maximum medium cores and 4 maximum big cores
    # for the frequency we consider that 1 means minimum or idle, 2 means half frequency, and 3 means maximum frequency
    # On this datas we vary the number of thread, for this reason all value are 2, supposing that cpu running the thread are on maximum frequency. 
    # For now we don't add equivalent values, we will add them later if possible, but we consider that on the same socket the core on which the thread is pinned does not matter. 
    # for exemple  (3,0,0,0,0,0, 0,0,0, 0,0,0,0), # could be equivalent to  (0,3,0,0,0,0, 0,0,0, 0,0,0,0)
    """
    configurations = ["idl" ,      
    "1m-0", "1h-0", "1-0",                                
    "2m-0",  "2h-0"  , "2-0",                
    "3m-0",  "3h-0"  , "3-0",   

    "2-1m" ,  "2-1h",   "2m-1m",     
    "2h-1h", "2-1",                 
    "1-1m" , "1-1h", 

    "1m-1m", "1h-1h",   "1-1",               
    "1-2m", "1-2h",   

    "1m-2m", "1h-2h", "1-2" ,                               
    "0-1m",   "0-1h", "0-1",                        
    "0-2m",  "0-2h" , "0-2"  ]                                                                                                                                           
    """
    file.write("0,0,0,0,0,0, 0,0,0, 0,0,0,0\n" 
        "1,0,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "2,0,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "1,1,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "2,2,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "1,1,1,0,0,0, 0,0,0, 0,0,0,0\n"
        "2,2,2,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,3,0,0,0, 0,0,0, 0,0,0,0\n"
        
        "3,3,0,0,0,0, 0,0,0, 1,0,0,0\n"
        "3,3,0,0,0,0, 0,0,0, 2,0,0,0\n"
        "1,1,0,0,0,0, 0,0,0, 1,0,0,0\n"
        "2,2,0,0,0,0, 0,0,0, 2,0,0,0\n"
        "3,3,0,0,0,0, 0,0,0, 3,0,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 1,0,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 2,0,0,0\n"
        "1,0,0,0,0,0, 0,0,0, 1,0,0,0\n"
        "2,0,0,0,0,0, 0,0,0, 2,0,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 3,0,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 1,1,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 2,2,0,0\n"

        "1,0,0,0,0,0, 0,0,0, 1,1,0,0\n"
        "2,0,0,0,0,0, 0,0,0, 2,2,0,0\n"
        "3,0,0,0,0,0, 0,0,0, 3,3,0,0\n"

        "0,0,0,0,0,0, 0,0,0, 1,0,0,0\n" 
        "0,0,0,0,0,0, 0,0,0, 2,0,0,0\n"
        "0,0,0,0,0,0, 0,0,0, 3,0,0,0\n"  

        "0,0,0,0,0,0, 0,0,0, 1,1,0,0\n" 
        "0,0,0,0,0,0, 0,0,0, 2,2,0,0\n"
        "0,0,0,0,0,0, 0,0,0, 3,3,0,0\n"  

        )

with open('machine_learning_datas/configurations_google_pixel_format.txt','w') as file:
    print("--- writing the configurations for google pixel in format (l,l,l,l,l,l,m,b)")
    #  we have 4 maximum little cores, 4  big cores"
    # we also have same considerations as above with the generic configuration
    file.write("0,0,0,0,0,0,0,0\n" 
        "1,0,0,0,0,0,0,0\n"
        "2,0,0,0,0,0,0,0\n"
        "3,0,0,0,0,0,0,0\n"
        "1,1,0,0,0,0,0,0\n"
        "2,2,0,0,0,0,0,0\n"
        "3,3,0,0,0,0,0,0\n"
        "1,1,1,0,0,0,0,0\n"
        "2,2,2,0,0,0,0,0\n"
        "3,3,3,0,0,0,0,0\n"
        
        "3,3,0,0,1,0,0,0\n"
        "3,3,0,0,2,0,0,0\n"
        "1,1,0,0,1,0,0,0\n"
        "2,2,0,0,2,0,0,0\n"
        "3,3,0,0,3,0,0,0\n"
        "3,0,0,0,1,0,0,0\n"
        "3,0,0,0,2,0,0,0\n"
        "1,0,0,0,1,0,0,0\n"
        "2,0,0,0,2,0,0,0\n"
        "3,0,0,0,3,0,0,0\n"
        "3,0,0,0,1,1,0,0\n"
        "3,0,0,0,2,2,0,0\n"

        "1,0,0,0,1,1,0,0\n"
        "2,0,0,0,2,2,0,0\n"
        "3,0,0,0,3,3,0,0\n"

        "0,0,0,0,1,0,0,0\n" 
        "0,0,0,0,2,0,0,0\n"
        "0,0,0,0,3,0,0,0\n"  

        "0,0,0,0,1,1,0,0\n" 
        "0,0,0,0,2,2,0,0\n"
        "0,0,0,0,3,3,0,0\n"  
        )


with open('machine_learning_datas/phone_energy.txt','w') as file:
    print("--- writing the phone energy")
    counter = 1
    for value in phone_energy:
            file.write(str(value))
            if (counter < len(phone_energy)):
                file.write('\n')
                counter = counter + 1

with open('machine_learning_datas/phone_power.txt','w') as file:
    print("--- writing the phone power")
    counter = 1
    for value in phone_energy:
        file.write(str(value))
        if (counter < len(phone_energy)):
            file.write('\n')
            counter = counter + 1

with open('machine_learning_datas/workload_computed.txt','w') as file:
    print("--- writing the workload comuted")
    counter = 1
    for value in workload:
        file.write(str(value))
        if (counter < len(workload)):
            file.write('\n')
            counter = counter + 1
    

with open('machine_learning_datas/ratio_energy_by_worload.txt','w') as file:
    print("--- writing the ratio energy by workload")
    ratio = np.divide(phone_energy,workload)
    counter = 1
    for value in ratio:
        file.write(str(value))
        if (counter < len(ratio)):
            file.write('\n')
            counter = counter + 1
  
configurations_for_automatisation = [[0,0,0,0,0,0,0,0], 
        [1,0,0,0,0,0,0,0],
        [2,0,0,0,0,0,0,0],
        [3,0,0,0,0,0,0,0],
        [1,1,0,0,0,0,0,0],
        [2,2,0,0,0,0,0,0],
        [3,3,0,0,0,0,0,0],
        [1,1,1,0,0,0,0,0],
        [2,2,2,0,0,0,0,0],
        [3,3,3,0,0,0,0,0],
        
        [3,3,0,0,1,0,0,0],
        [3,3,0,0,2,0,0,0],
        [1,1,0,0,1,0,0,0],
        [2,2,0,0,2,0,0,0],
        [3,3,0,0,3,0,0,0],
        [3,0,0,0,1,0,0,0],
        [3,0,0,0,2,0,0,0],
        [1,0,0,0,1,0,0,0],
        [2,0,0,0,2,0,0,0],
        [3,0,0,0,3,0,0,0],
        [3,0,0,0,1,1,0,0],
        [3,0,0,0,2,2,0,0],

        [1,0,0,0,1,1,0,0],
        [2,0,0,0,2,2,0,0],
        [3,0,0,0,3,3,0,0],

        [0,0,0,0,1,0,0,0], 
        [0,0,0,0,2,0,0,0],
        [0,0,0,0,3,0,0,0],  

        [0,0,0,0,1,1,0,0], 
        [0,0,0,0,2,2,0,0],
        [0,0,0,0,3,3,0,0]]
configuration_human_readable = ["0000-0000", 
        "1000-0000",
        "2000-0000",
        "3000-0000",
        "1100-0000",
        "2200-0000",
        "3300-0000",
        "1110-0000",
        "2220-0000",
        "3330-0000",
        
        "3300-1000",
        "3300-2000",
        "1100-1000",
        "2200-2000",
        "3300-3000",
        "3000-1000",
        "3000-2000",
        "1000-1000",
        "2000-2000",
        "3000-3000",
        "3000-1100",
        "3000-2200",

        "1000-1100",
        "2000-2200",
        "3000-3300",

        "0000-1000", 
        "0000-2000",
        "0000-3000",  

        "0000-1100", 
        "0000-2200",
        "0000-3300"]

data_transposed = []
data_transposed.append(configuration_human_readable)
data_transposed.append(configurations_for_automatisation)


data = (np.array(data_transposed, dtype=object)).T

header = ["configurations", "samsung galaxy format"]
print ("final data", data)

with open('machine_learning_datas/input_configurations.csv','w') as file:
    counter = 1
    for title in header:
        file.write(title)
        if (counter < len(header)):
            file.write(',')
            counter = counter + 1
    file.write('\n')
    
    line = []
    for line in data:
        counter_value = 1
        for value in line:
            file.write(str(value).replace(",", "-"))
            if (counter_value < len(line)):
                file.write(',')
                counter_value = counter_value + 1
        file.write('\n')

# for machine learning process I did not use the "mouse" results

"""
txt = ["PS5", "is", "currently", "unavailable", "in", "India"]

with open('shows.csv','w') as file:
    for line in txt:
        file.write(line)
        file.write('\n')

plt.bar(configurations,phone_power, width, label='With new usb cable')


plt.bar(configurations,phone_energy, width=0.4)

#################################################


plt.bar(configurations,np.divide(workload,100000000000), width=0.4)


plt.bar(configurations,np.divide(phone_energy,workload), width=0.4)

"""

