from ctypes import sizeof
import matplotlib.pyplot as plt
import numpy as np
import math
import os


           #-->12: (2 little, 1 big)             //pour voir si les littles mènent la danse


"""
configurations = ["1_p-Big_stp","1-Big_stp", "1-0","0-1_b","0-1_B"]
workload = [ 1.45, 1.45, 1.60362, 7.03,  7.69 ]
energy_consumed = [ 33.7, 34.4,  33.3, 19.4,  26.1  ]
"""


"""
number_of_cpus = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0" , "5-0.", "6-0", "6-0.",                          "6-1",    "6-2"             ,"0-1-0",  "0-0-1"  ,"0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,    53629.51, 58756.40, 56820.57,   66053.62,  64353.13,           106210.78 , 123823.21,       66877.5,77632.30, 115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15,    1661.97,   1918.21,   1871.11,                2970.56 ,  3397.90,          1938.45, 2214.51, 3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,     6.906,  7.3213,     9.223199,   10.85176,   11.1178,             17.08829 ,  21.384681,       6.7442,  7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          
"""


"""
configurations = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0" , "5-0.", "6-0", "6-0.",                          "6-1",    "6-2"                       ,"0-1-0", "0-0-1", "0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,    53629.51, 58756.40, 56820.57,   66053.62,  64353.13,                 106210.78 , 123823.21,             66877.5, 77632.30, 115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15,    1661.97,   1918.21,   1871.11,                2970.56 ,  3397.90,                 1938.45,2214.51,3179.93,                2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,     6.906,  7.3213,     9.223199,   10.85176,   11.1178,                       17.08829 ,  21.384681,       6.7442,  7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          
"""





""""
configurations = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0", "6-0",              "6-1",    "6-2"             ,"0-1-0", "0-0-1", "0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,    53629.51, 58756.40, 66053.62,     106210.78 , 123823.21,       66877.5,  77632.30,  115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15, 1918.21,         2970.56 ,  3397.90,          1938.45, 2214.51,  3179.93,          2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,     6.906,  7.3213, 10.85176,          17.08829 ,  21.384681,       6.7442,  7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          

phone_energy_for_validation_of_0_5_0_6 = [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,        53629.51, 56820.57,     64353.13,                 106210.78 , 123823.21,       66877.5, 77632.30,  115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power_for_validation_of_0_5_0_6 = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,      1572.76,  1661.97,    1871.11,                       2970.56 ,  3397.90,          1938.45, 2214.51, 3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload_for_validation_of_0_5_0_6  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,        6.906,     9.223199,    11.1178,                       17.08829 ,  21.384681,       6.7442, 7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          
"""
# I used the final datas without error, the one above, generated for validation of configurations 0-5 and 0-6 
configurations = [ "idle"       ,"1-0",  "2-0",  "3-0",                "4-0", "5-0", "6-0",                         "6-1-0",    "6-1-1"                 ,"0-1-0",  "0-0-1", "0-1-1",                   "1-1",  "2-1", "3-1",              "1-1-1" ]                                                        
phone_energy = [ 11047.73,       29197.24, 38598.14, 46077.78,        53629.51, 56820.57,  64353.13,         106210.78 , 123823.21,                  66877.5, 77632.30, 115136.69,              73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power=    [ 336.07,        879.245 , 1146.12,  1357.74  ,     1572.76,  1661.97,    1871.11,            2970.56 ,  3397.90,                   1938.45, 2214.51, 3179.93,             2106.08,    2267.14 , 2614.01             , 2717.37]                  
workload=       [ 0.9,        1.60362,    3.368,  4.9563   ,        6.906,     9.223199,    11.1178,               17.08829 ,  21.384681,         6.7442, 7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          
workload = np.multiply(workload,10**11)  # Note !!! In previous machine learning models I did not realize this multiplication. 
                                         # It is mandatory to restart generation process if I need to compare with automatic experiment, in which I printed the exact value of workload. 
phone_energy=np.divide(phone_energy,10**3)

cwd = os.getcwd()
print ("current directory = ")
print(cwd)
dir = os.path.join(cwd,"machine_learning_datas")
if not os.path.exists(dir):
    os.mkdir(dir)

with open('machine_learning_datas/configurations_user_friendly.txt','w') as file:
    print("--- writing the phone configurations in user friendly format")
    counter = 1
    for value in configurations:
            file.write(str(value))
            if (counter < len(phone_energy)):
                file.write('\n')
                counter = counter + 1



with open('machine_learning_datas/configurations_generic_format.txt','w') as file:
    print("--- writing the configurations for generic phone in format (l,l,l,l,l,l,m,m,m,b,b,b,b)")
    #for generic phone, we have 6 maximum little cores, 3 maximum medium cores and 4 maximum big cores
    # for the frequency we consider that 1 means minimum or idle, 2 means half frequency, and 3 means maximum frequency
    # On this datas we vary the number of thread, for this reason all value are 3, supposing that cpu running the thread are on maximum frequency. 
    # For now we don't add equivalent values, we will add them later if possible,
    #  but we consider that on the same socket the core on which the thread is pinned does not matter. 
    # for exemple  (3,0,0,0,0,0, 0,0,0, 0,0,0,0), # could be equivalent to  (0,3,0,0,0,0, 0,0,0, 0,0,0,0)
    file.write("0,0,0,0,0,0, 0,0,0, 0,0,0,0\n" 
        "3,0,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,0,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,3,0,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,3,3,0,0, 0,0,0, 0,0,0,0\n"
        "3,3,3,3,3,0, 0,0,0, 0,0,0,0\n"
        "3,3,3,3,3,3, 0,0,0, 0,0,0,0\n"
        "3,3,3,3,3,3, 3,0,0, 0,0,0,0\n"##
        "3,3,3,3,3,3, 3,0,0, 3,0,0,0\n"##
        "0,0,0,0,0,0, 3,0,0, 0,0,0,0\n"##
        "0,0,0,0,0,0, 0,0,0, 3,0,0,0\n"##
        "0,0,0,0,0,0, 3,0,0, 3,0,0,0\n"##
        "3,0,0,0,0,0, 3,0,0, 0,0,0,0\n"##
        "3,3,0,0,0,0, 3,0,0, 0,0,0,0\n"##
        "3,3,3,0,0,0, 3,0,0, 0,0,0,0\n"##
        "3,0,0,0,0,0, 3,0,0, 3,0,0,0\n")##

with open('machine_learning_datas/configurations_google_pixel_format.txt','w') as file:
    print("--- writing the configurations for google pixel in format (l,l,l,l,l,l,m,b)")
    #  we have 6 maximum little cores, 1 medium core and 1  big core"
    # we also have same considerations as above with the generic configuration
    file.write("0,0,0,0,0,0, 0,0\n" 
        "3,0,0,0,0,0, 0,0\n"
        "3,3,0,0,0,0, 0,0\n"
        "3,3,3,0,0,0, 0,0\n"
        "3,3,3,3,0,0, 0,0\n"
        "3,3,3,3,3,0, 0,0\n"
        "3,3,3,3,3,3, 0,0\n"
        "3,3,3,3,3,3, 3,0\n"
        "3,3,3,3,3,3, 3,3\n"
        "0,0,0,0,0,0, 3,0\n"
        "0,0,0,0,0,0, 3,0\n"
        "0,0,0,0,0,0, 3,3\n"
        "3,0,0,0,0,0, 3,0\n"
        "3,3,0,0,0,0, 3,0\n"
        "3,3,3,0,0,0, 3,0\n"
        "3,0,0,0,0,0, 3,3\n")

with open('machine_learning_datas/phone_energy.txt','w') as file:
    print("--- writing the phone energy")
    counter = 1
    for value in phone_energy:
            file.write(str(value))
            if (counter < len(phone_energy)):
                file.write('\n')
                counter = counter + 1

with open('machine_learning_datas/phone_power.txt','w') as file:
    print("--- writing the phone power")
    counter = 1
    for value in phone_energy:
        file.write(str(value))
        if (counter < len(phone_energy)):
            file.write('\n')
            counter = counter + 1

with open('machine_learning_datas/workload_computed.txt','w') as file:
    print("--- writing the workload comuted")
    counter = 1
    for value in workload:
        file.write(str(value))
        if (counter < len(workload)):
            file.write('\n')
            counter = counter + 1
    

with open('machine_learning_datas/ratio_energy_by_worload.txt','w') as file:
    print("--- writing the ratio energy by workload")
    ratio = np.divide(phone_energy,workload)
    counter = 1
    for value in ratio:
        file.write(str(value))
        if (counter < len(ratio)):
            file.write('\n')
            counter = counter + 1
  

# for machine learning process I did not use the "mouse" results



#### Taking into account the real value of frequency 
# for a particular dictionnary this function transform the observation X[i], to reflect exact values of frequency
# possible dictionnaries can be: 
#  1 - dictionnary of experiment on google pixel 4a 5g on little socket, 
#          0 ->  0 
#          1 -> 576000   (it is the minimum usable frequency on little core on google pixel 4a 5g)
#          2 -> 1363200  (it is the mid frequency on little core on google pixel 4a 5g)
#          3 -> 1804800  (it is the max frequency on little core on google pixel 4a 5g)
freq_dict_of_little_socket_google_pixel = { 0: 0, 1: 576000,  2: 1363200,  3: 1804800  }
#  2 - dictionnary of experiments on google pixel 4a 5g  on medium socket, 
#          0 -> 0
#          1 -> 652800   (it is the minimum usable frequency on big core on google pixel 4a 5g)
#          2 -> 1478400  (it is the mid frequency on big core on google pixel 4a 5g)
#          3 -> 2208000  (it is the max frequency on big core on google pixel 4a 5g )
freq_dict_of_medium_socket_google_pixel = { 0: 0, 1: 652800,  2: 1478400,  3: 2208000  }
#### Before continue I put there a big warning because, I need to reconsider the fact that google pixel à three types of cores (medium, little, big cores)
####
#  3 - dictionnary of experiment on google pixel 4a 5g on big socket, 
#          0 ->  0 
#          1 -> 806400   (it is the minimum usable frequency on big core on google pixel 4a 5g)
#          2 -> 1766400   (it is the mid frequency on big core on google pixel 4a 5g)
#          3 -> 2400000  (it is the max frequency on big core on google pixel 4a 5g )
freq_dict_of_big_socket_google_pixel = { 0: 0, 1: 806400,  2: 1766400,  3: 2400000  }

def trans_form_X_generic_to_have_real_values_of_frequency(X, freq_dict_of_little_socket , freq_dict_of_medium_socket, freq_dict_of_big_socket):
    result = []
    for x in X:
        temp_x = [] 
        for i in range(0,6):
            temp_x.append(freq_dict_of_little_socket[x[i]])
        for i in range(6,9):
            temp_x.append(freq_dict_of_medium_socket[x[i]])
        for i in range(9,13):
            temp_x.append(freq_dict_of_big_socket[x[i]])
        result.append(temp_x)
    return result

def trans_form_X_google_format_to_have_real_values_of_frequency(X, freq_dict_of_little_socket , freq_dict_of_medium_socket, freq_dict_of_big_socket):
    result = []
    for x in X:
        temp_x = [] 
        for i in range(0,6):
            temp_x.append(freq_dict_of_little_socket[x[i]])
        temp_x.append(freq_dict_of_medium_socket[x[6]])
        temp_x.append(freq_dict_of_big_socket[x[7]])
        result.append(temp_x)
    return result

def read_configuration_X(file_path):
    print("---> Reading the data set file X in generic format") 
    X = []
    with open(file_path) as fp:
        line = fp.readline()
        cnt = 1
        while line:
            #print("reading line {}: {}".format(cnt, line.strip()))
            str_X_line = line.split(',')
            #print ("arrays as string : ", str_X_line)
            X_line = [int(numeric_string) for numeric_string in str_X_line]
            #print("resulted array : ", X_line)
            X.append(X_line)
            #print("after adding it to X : ", X)
            line = fp.readline()
            cnt += 1
    return X
# now writing configurations in generic format with exact value of frequency
with open('machine_learning_datas/configurations_generic_format_exact_frequency_values.txt','w') as file:
    print("--- writing the configurations with exact values of frequency in generic format")
    _google_pixel_4a_5g_varying_number_of_threads_generic = read_configuration_X("machine_learning_datas/configurations_generic_format.txt")
    _google_pixel_4a_5g_varying_number_of_threads_generic_exact_freq = trans_form_X_generic_to_have_real_values_of_frequency(
         _google_pixel_4a_5g_varying_number_of_threads_generic,
         freq_dict_of_little_socket_google_pixel,
         freq_dict_of_medium_socket_google_pixel,
         freq_dict_of_big_socket_google_pixel
    )

    counter_line = 1
    line = []
    for line in _google_pixel_4a_5g_varying_number_of_threads_generic_exact_freq:
        counter_value = 1
        for value in line:
            file.write(str(value))
            if (counter_value < len(line)):
                file.write(',')
                counter_value = counter_value + 1      
        if (counter_line < len(_google_pixel_4a_5g_varying_number_of_threads_generic_exact_freq)):
            file.write('\n')
            counter_line = counter_line + 1

    
  

# now writing configurations in google pixel format with exact value of frequency
with open('machine_learning_datas/configurations_google_pixel_format_exact_frequency_values.txt','w') as file:
    print("--- writing the configurations with exact values of frequency in google format")
    _google_pixel_4a_5g_varying_number_of_threads_google_pixel_format = read_configuration_X("machine_learning_datas/configurations_google_pixel_format.txt")
    _google_pixel_4a_5g_varying_number_of_threads_google_pixel_format_exact_freq = trans_form_X_google_format_to_have_real_values_of_frequency(
         _google_pixel_4a_5g_varying_number_of_threads_google_pixel_format,
         freq_dict_of_little_socket_google_pixel,
         freq_dict_of_medium_socket_google_pixel,
         freq_dict_of_big_socket_google_pixel
    )
 

    counter_line = 1
    line = []
    for line in _google_pixel_4a_5g_varying_number_of_threads_google_pixel_format_exact_freq:
        counter_value = 1
        for value in line:
            file.write(str(value))
            if (counter_value < len(line)):
                file.write(',')
                counter_value = counter_value + 1      
        if (counter_line < len(_google_pixel_4a_5g_varying_number_of_threads_google_pixel_format_exact_freq)):
            file.write('\n')
            counter_line = counter_line + 1
  


# now writing all valuable datas on csv format

data_transposed = []
data_transposed.append(configurations)
data_transposed.append(_google_pixel_4a_5g_varying_number_of_threads_generic)
data_transposed.append(_google_pixel_4a_5g_varying_number_of_threads_generic_exact_freq)
data_transposed.append(_google_pixel_4a_5g_varying_number_of_threads_google_pixel_format)
data_transposed.append(_google_pixel_4a_5g_varying_number_of_threads_google_pixel_format_exact_freq)
data_transposed.append(phone_energy)
data_transposed.append(phone_power)
data_transposed.append(workload)
data_transposed.append(list(np.divide(phone_energy,workload)))
print( data_transposed)
data = (np.array(data_transposed, dtype=object)).T

header = ["configurations", "generic format", "exact frequency", 
"google pixel format", "exact frequencies", 
"phone energy", "phone power", "workload", "energy by workload"]
print ("final data", data)

with open('machine_learning_datas/summary.csv','w') as file:
    counter = 1
    for title in header:
        file.write(title)
        if (counter < len(header)):
            file.write(',')
            counter = counter + 1
    file.write('\n')
    
    line = []
    for line in data:
        counter_value = 1
        for value in line:
            file.write(str(value).replace(",", "-"))
            if (counter_value < len(line)):
                file.write(',')
                counter_value = counter_value + 1
        file.write('\n')
#data.tofile('summary.txt', sep = ',')
#np.savetxt('summary.csv', data, delimiter=',') 






"""
txt = ["PS5", "is", "currently", "unavailable", "in", "India"]

with open('shows.csv','w') as file:
    for line in txt:
        file.write(line)
        file.write('\n')

plt.bar(configurations,phone_power, width, label='With new usb cable')


plt.bar(configurations,phone_energy, width=0.4)

#################################################


plt.bar(configurations,np.divide(workload,100000000000), width=0.4)


plt.bar(configurations,np.divide(phone_energy,workload), width=0.4)

"""

