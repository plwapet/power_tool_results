import matplotlib.pyplot as plt
import numpy as np
import math



           #-->12: (2 little, 1 big)             //pour voir si les littles mènent la danse


"""
number_of_cpus = ["1_p-Big_stp","1-Big_stp", "1-0","0-1_b","0-1_B"]
workload = [ 1.45, 1.45, 1.60362, 7.03,  7.69 ]
energy_consumed = [ 33.7, 34.4,  33.3, 19.4,  26.1  ]
"""


"""
number_of_cpus = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0" , "5-0.", "6-0", "6-0.",                          "6-1",    "6-2"                  ,"0-1-0",  0-0-1   , "0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,    53629.51, 58756.40, 56820.57,   66053.62,  64353.13,                 106210.78 , 123823.21,       66877.5,   77632.30   ,115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15,    1661.97,   1918.21,   1871.11,                2970.56 ,  3397.90,          1938.45,  2214.51  ,3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,     6.906,  7.3213,     9.223199,   10.85176,   11.1178,             17.08829 ,  21.384681,       6.7442,  7.64552224  ,13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          
"""






number_of_cpus = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0", "6-0",              "6-1",    "6-2"             ,"0-1-0",  "0-0-1"   ,"0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,    53629.51, 58756.40, 66053.62,     106210.78 , 123823.21,       66877.5,  77632.30,    115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15, 1918.21,         2970.56 ,  3397.90,          1938.45, 2214.51,  3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,     6.906,  7.3213, 10.85176,          17.08829 ,  21.384681,       6.7442, 7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          






fig = plt.figure()

width = 0.35  
plt.bar(number_of_cpus,phone_power, width, label='With new usb cable')

# Add title and axis names
plt.title('Avg power used by the phone according to the configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel('AVG Power')

#plt.xticks(fontsize=8)
fig.autofmt_xdate()

plt.savefig("Google_pixel_power_absorbed_according_to_configuration_cpu_charge_stop_level_50.png")
plt.legend(loc='upper left')
plt.clf()
plt.cla()
plt.close()
########################################################################################

fig = plt.figure()
plt.bar(number_of_cpus,phone_energy, width=0.4)
 
# Add title and axis names
plt.title('Energy consumed according to the \n number of configuration')
plt.xlabel('Number of CPUs')
plt.ylabel('Battery cpu usage')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Google_pixel_energy_usage_according_to_configuration_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()

#################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(workload,100000000000), width=0.4)

# Add title and axis names
plt.title('New computed Workload according to the configuration')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Workload ($\times 10E11$)')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Google_pixel_workload_according_to_configuration_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()
################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy,workload), width=0.4)
# Add title and axis names
plt.title('Energy efficiency Energy/Workload according to \n the configuration (lower is better)')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Battery cpu/Workload ($\times 10E-11$)')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Google_pixel_ratio_energy_by_workload_according_to_configuration_cpu_charge_stop_level_50.png")

plt.clf()
plt.cla()
plt.close()

################################################





"""
number_of_cpus = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0" , "5-0.", "6-0", "6-0.",                          "6-1",    "6-2"             ,"0-1-0",  "0-0-1"  ,"0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,    53629.51, 58756.40, 56820.57,   66053.62,  64353.13,           106210.78 , 123823.21,       66877.5,77632.30, 115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15,    1661.97,   1918.21,   1871.11,                2970.56 ,  3397.90,          1938.45, 2214.51, 3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,     6.906,  7.3213,     9.223199,   10.85176,   11.1178,             17.08829 ,  21.384681,       6.7442,  7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          
"""




phone_energy_for_validation_of_0_5_0_6 = [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,        53629.51, 56820.57,     64353.13,        106210.78 , 123823.21,       66877.5,77632.30,115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power_for_validation_of_0_5_0_6 = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,      1572.76,  1661.97,    1871.11,                2970.56 ,  3397.90,          1938.45,2214.51, 3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload_for_validation_of_0_5_0_6  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,        6.906,     9.223199,    11.1178,          17.08829 ,  21.384681,       6.7442, 7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          


fig = plt.figure()

width = 0.35  
plt.bar(number_of_cpus,phone_power_for_validation_of_0_5_0_6, width, label='With new usb cable')

# Add title and axis names
plt.title('Avg power used by the phone according to configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel('AVG Power')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Google_pixel_power_absorbed_according_to_configuration_cpu_charge_stop_level_50_validation_of_0_5_0_6_.png")
plt.legend(loc='upper left')
plt.clf()
plt.cla()
plt.close()
########################################################################################

fig = plt.figure()
plt.bar(number_of_cpus,phone_energy_for_validation_of_0_5_0_6, width=0.4)
 
# Add title and axis names
plt.title('Energy consumed according to the  configuration')
plt.xlabel('Number of CPUs')
plt.ylabel('Battery cpu usage')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Google_pixel_energy_usage_according_to_configuration_cpu_charge_stop_level_50_validation_of_0_5_0_6.png")

plt.clf()
plt.cla()
plt.close()

#################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(workload_for_validation_of_0_5_0_6,100000000000), width=0.4)

# Add title and axis names
plt.title('New computed Workload according to the configuration')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Workload ($\times 10E11$)')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Google_pixel_workload_according_to_configuration_cpu_charge_stop_level_50_validation_of_0_5_0_6.png")

plt.clf()
plt.cla()
plt.close()
################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy_for_validation_of_0_5_0_6,workload_for_validation_of_0_5_0_6), width=0.4)
# Add title and axis names
plt.title('Energy efficiency (Energy/Workload) according to \n the configuration (lower is better)')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Battery cpu/Workload ($\times 10E-11$)')
#plt.xticks(fontsize=8)
fig.autofmt_xdate()
plt.savefig("Google_pixel_ratio_energy_by_workload_according_to_configuration_cpu_charge_stop_level_50_validation_of_0_5_0_6.png")

plt.clf()
plt.cla()
plt.close()

################################################


######################################################## plotting graphs for the paper. 
# Showing that the number of threads deployed on cores influence the energy efficiency: 
#  Dans l'expérimentation illustrée par la figure \ref{fig:numberOfThreadsInfluenceEnergyEfficiency}
#  Nous avons repris l'expérience précédente mais en augmentant le nombre de thread exécutant la même tâche. 
# Le nombre de thread ainsi déployé sur le little socket variait de 1 à 6
# De manière générale paralléliser les batch consuming tasks rend beaucoup plus efficace. 
# Mais certaines configuration comme celle dans laquelle nous avons 5 threads peuvent ne pas suivre la même tendance.  



number_of_cpus = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0", "6-0",              "6-1",    "6-2"             ,"0-1-0",  "0-0-1"   ,"0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,    53629.51, 58756.40, 66053.62,     106210.78 , 123823.21,       66877.5,  77632.30,    115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15, 1918.21,         2970.56 ,  3397.90,          1938.45, 2214.51,  3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,     6.906,  7.3213, 10.85176,          17.08829 ,  21.384681,       6.7442, 7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          







#frequency = [806400, 1766400, 2400000 ]
number_of_threads = ["1", "2", "3", "4", "5", "6"]                                                        
phone_energy= [29197.24, 38598.14, 46077.78,    53629.51, 58756.40, 66053.62  ]   
workload  = [1.60362,    3.368,  4.9563   ,     6.906,  7.3213, 10.85176 ]    
fontsize_in_paper = 20
bar_width = 0.4
label_format = '{:,.1f}'
fig, ((workload_fig, phone_energy_fig, energy_efficiency_fig)) = plt.subplots(nrows= 1, ncols = 3,  figsize=(15, 7), sharex=True)
#frequency[:] = [round(x / 1E+6, 2) for x in frequency]                                                                                                     
phone_energy[:] = [round(x / 1000, 2) for x in phone_energy]   
energy_efficiency = np.divide(workload, phone_energy) #*****# workload  (*1e11)   error = +- 0.0117 e 11 ;  phone energy is now in mAh , error =  +- 0.773  mAh
energy_efficiency_error =  [ 0.0117/e_ +  0.773*w_ / (e_ ** 2)  for w_, e_ in zip(workload, phone_energy)]  #*****# with error propagation, we have delta(energy_efficiency)/energy_efficiency = delta(workload)/workload + delta(energy)/energy
                           #*****#  so delta(energy_efficiency) = delta(worklaoad)/energy + delta(energy)*workload/energy^2
energy_efficiency[:] = [round(x*10, 2) for x in energy_efficiency]   #   energy efficiency has been multiplied by 10
energy_efficiency_error[:] = [x*10 for x in energy_efficiency_error]     #*****#  same thing for the error                                                                                                   

energy_efficiency_fig.bar(number_of_threads,  energy_efficiency, yerr= energy_efficiency_error, width=bar_width, color='gray', edgecolor='black', hatch='//')
energy_efficiency_fig.set_title('Energy efficiency\n ' + r'($\times 10e11$)', fontsize = fontsize_in_paper)
energy_efficiency_fig.set_xticklabels(number_of_threads, fontsize = fontsize_in_paper)
energy_efficiency_fig.set_yticklabels([label_format.format(x) for x in energy_efficiency_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)

label_format = '{:,.0f}'
workload[:] = [round(x, 2 ) for x in workload]    
workload_error =  [0.0117] * len(number_of_threads)     #*****#                                                                                          
workload_fig.bar(number_of_threads, workload, width= bar_width, yerr = workload_error, color='gray', edgecolor='black', hatch='//')
workload_fig.set_title('Number of operations\n' + r'($\times 10e11$)', fontsize = fontsize_in_paper)
workload_fig.set_xticklabels(number_of_threads, fontsize = fontsize_in_paper)
workload_fig.set_yticklabels([label_format.format(x) for x in workload_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)


#plt.xticks(fontsize=8)

phone_energy[:] = [round(x , 2 ) for x in phone_energy]  # I alrady divided by 1000   
energy_error =    [0.773] * len(number_of_threads)     #*****#                                                                                             
phone_energy_fig.bar(number_of_threads, phone_energy, yerr = energy_error, width=bar_width, color='gray', edgecolor='black', hatch='//')  # I divided the energy by 1000  because Moonson made a mistakes in the output, they gives energy in mAh 1000 times.
phone_energy_fig.set_title('Energy consumed\n' + r'(mAh)', fontsize = fontsize_in_paper)
phone_energy_fig.set_xticklabels(number_of_threads, fontsize = fontsize_in_paper)
phone_energy_fig.set_yticklabels([label_format.format(x) for x in phone_energy_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)




#fig.suptitle("Workload, Energy and Energy efficiency according to the number of threads")
fig.supxlabel("Number of threads on the little socket", fontsize = fontsize_in_paper)
#plt.gcf().autofmt_xdate()

plt.savefig("Number_of_threads_influence_energy_efficiency.png")
plt.clf()
plt.cla()
plt.close()




# Showing that the phone model influence the energy efficiency: 
#  Pour observer son influence sur l'efficacité nous avons mené une expérimentation. Et la figure \ref{fig:phoneModelInfluenceEnergyEfficiency} illustre les résultat obtenus.
#  En effet, Nous avons considéré deux téléphone le google pixel (en black) et le Samsung galaxay (en gray hatched)
# Sur chacun d'eux nous avons démarré un puis deux threads parallèles : ce sont les configuration 1-0 et 2-0. 
# Nous avons ensuite démarré un thread sur son big core. c'est la configuration 0-1.
# Nous pouvons noter au moins deux comportement différents d'un téléphone à l'autre:
# le fait de paralléliser sur le google pixel a augmenter l'efficacité de ratio_1 % sur le google alors que cette augmentation a seulement été de  % sur le samsung. 
# Le fait d'utiliser le Big core est beaucoup plus efficace que de paralléliser sur le samsung alors que ce n'est pas le cas sur le google pixel. 
# 
#  


#####  Google Pixel datas. 
number_of_cpus = ["mouse",  "idle"       ,"1-0",  "2-0",  "3-0",       "4-0", "5-0", "6-0",              "6-1",    "6-2"             ,"0-1-0",  "0-0-1"   ,"0-2",              "1-1",  "2-1", "3-1",      "1-2" ]                                                        
phone_energy= [ 1308, 11047.73,       29197.24, 38598.14, 46077.78,    53629.51, 58756.40, 66053.62,     106210.78 , 123823.21,       66877.5,  77632.30,    115136.69,        73048.75, 79000.24 , 92143.93     , 96571.58 ]                                                                                                   
phone_power = [ 40.16, 336.07,        879.245 , 1146.12,  1357.74  ,   1572.76, 1718.15, 1918.21,         2970.56 ,  3397.90,          1938.45, 2214.51,  3179.93,        2106.08,    2267.14 , 2614.01   , 2717.37]                  
workload  = [ 0.1,         0.9,        1.60362,    3.368,  4.9563   ,     6.906,  7.3213, 10.85176,          17.08829 ,  21.384681,       6.7442, 7.64552224, 13.210451922,     8.47040,  10.3032,    12.38267        , 12.72335]          

# paper data extracted
configuration = ["1-0", "2-0", "0-1"]                                                        
google_phone_energy=  [29197.24, 38598.14, 77632.30 ]   
google_workload  = [1.60362,    3.368,  7.64552224 ]    
fontsize_in_paper = 20
bar_width = 0.4
label_format = '{:,.1f}'


ratio_1 = (google_workload[1]/google_phone_energy[1]  - google_workload[0]/google_phone_energy[0] )  / (google_workload[0]/google_phone_energy[0])         
print(" ratio 1 ", ratio_1)

##### Samsung datas 
number_of_cpus = ["mouse",  "idle" ,               "1-0",    "2-0",      "3-0",            "4-0",      "4-1",   "4-2",      "4-3",    "4-4",             "0-1",     "0-2",    "0-3" ,   "0-4",                  "2-1",   "3-1" ,    "1-1",                         "1-2", "1-3"       ]                                                        
phone_energy= [ 1308,     15437.75 ,         51208.79 , 54347.47,  57513.95   ,       74701.21,       98200.09 , 124222.42, 128746.44  , 145726.86,      94219.85 , 100720.96,  141336.4,   122949.4 ,         98711.42,  101212.13 ,  90216.11,                    110737,  111186.62    ]                                                                                                   
phone_power = [ 40.16,    1047.42   ,         2617.20, 2532.40 ,  1950.01   ,         2618.09,       2620.45 ,     2616.48,     38.35,  2620.32,             2620.1  , 2622.43,     2231.52 , 2623.83,                2619.02, 2620.84 ,   2620.50,                2621.93,   2618.84      ]                  
workload  = [ 0.1,        1 ,                1.25068,  2.688753,   4.02600  ,          5.25640  ,   8.44232736,    11.73684641,  13.9001248561 , 16.270943,    3.61959474 , 6.09298,  10.06644613, 11.9685747498 ,           6.14819, 7.27350446993,   4.550717,       7.632924365,   10.042245486   ]          

# paper data extracted
configuration = ["1-0", "2-0", "0-1"]                                                        
samsung_phone_energy=  [ 51208.79, 54347.47, 100720.96]   
samsung_workload  = [   1.25068,  2.688753, 3.61959474]    
fontsize_in_paper = 20
bar_width = 0.4
label_format = '{:,.1f}'

ratio_2 = (samsung_workload[1]/samsung_phone_energy[1]  - samsung_workload[0]/samsung_phone_energy[0] )  / (samsung_workload[0]/samsung_phone_energy[0])        
print(" ratio 2 ", ratio_2)

y = np.arange(len(configuration)) 

'''
fig, ax = plt.subplots()
google_pixel_bars = ax.bar(y - bar_width/2, TO_PLOT_1 , width, label='Google Pixel 4A 5G')
samsung_bars = ax.bar(y + bar_width/2, TO_PLOT_1, width, label='Samsung Galaxy S8')
ax.legend()
ax.bar_label(google_pixel_bars, padding=3)
ax.bar_label(samsung_bars, padding=3)
fig.tight_layout()
'''





fig, ((workload_fig, phone_energy_fig, energy_efficiency_fig)) = plt.subplots(nrows= 1, ncols = 3,  figsize=(15, 7), sharex=True)
################ energy efficiency subplot
## google pixel bars
google_phone_energy[:] = [round(x / 1000, 2) for x in google_phone_energy]   
google_energy_efficiency = np.divide(google_workload, google_phone_energy)  #*****# workload  (*1e11)   error = +- 0.0117 e 11 ;  phone energy is now in mAh , error =  +- 0.773  mAh
google_energy_efficiency_error =  [ 0.0117/e_ +  0.773*w_ / (e_ ** 2)  for w_, e_ in zip(google_workload, google_phone_energy)]  #*****# with error propagation, we have delta(energy_efficiency)/energy_efficiency = delta(workload)/workload + delta(energy)/energy
                           #*****#  so delta(energy_efficiency) = delta(worklaoad)/energy + delta(energy)*workload/energy^2
google_energy_efficiency[:] = [round(x*10, 2) for x in google_energy_efficiency]      #   energy efficiency has been multiplied by 10
google_energy_efficiency_error[:] = [x*10 for x in google_energy_efficiency_error]     #*****#  same thing for the error                     
google_pixel_ef_bars = energy_efficiency_fig.bar(y - bar_width/2, google_energy_efficiency , bar_width,  yerr=google_energy_efficiency_error, label='Google Pixel 4A 5G', color = "gray")  # in google_pixel_ef_bars,  "ef" is for energy efficiency
## samsung bars
samsung_phone_energy[:] = [round(x / 1000, 2) for x in samsung_phone_energy]   
samsung_energy_efficiency = np.divide(samsung_workload, samsung_phone_energy)  #*****# workload  (*1e11)   error = +-0.025953  e11 e 11 (the error has been computed based on repeated experiment, see the raw result file);  phone energy is now in mAh , error =  +- 0.773  mAh
samsung_energy_efficiency_error =  [ 0.025953/e_ +  0.773*w_ / (e_ ** 2)  for w_, e_ in zip(samsung_workload, samsung_phone_energy)]  #*****# with error propagation, we have delta(energy_efficiency)/energy_efficiency = delta(workload)/workload + delta(energy)/energy
                           #*****#  so delta(energy_efficiency) = delta(worklaoad)/energy + delta(energy)*workload/energy^2
samsung_energy_efficiency[:] = [round(x*10, 2) for x in samsung_energy_efficiency]   #   energy efficiency has been multiplied by 10
samsung_energy_efficiency_error[:] = [x*10 for x in samsung_energy_efficiency_error]     #*****#  same thing for the error        
samsung_ef_bars = energy_efficiency_fig.bar(y + bar_width/2, samsung_energy_efficiency,  bar_width, yerr = samsung_energy_efficiency_error, label='Samsung Galaxy S8', color='gray', edgecolor='black', hatch='//')
# setting the subplot
energy_efficiency_fig.set_title('Energy efficiency_\n ' + r'($\times 10e11$)', fontsize = fontsize_in_paper)
energy_efficiency_fig.set_xticks(y, configuration)
energy_efficiency_fig.set_xticklabels(configuration, fontsize = fontsize_in_paper)
energy_efficiency_fig.set_yticklabels([label_format.format(x) for x in energy_efficiency_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)


################ workload subplot
## google pixel bars
label_format = '{:,.0f}'
google_workload[:] = [round(x, 2 ) for x in google_workload]
google_pixel_w_bars =  workload_fig.bar(y - bar_width/2, google_workload , bar_width, label='Google Pixel 4A 5G', color = "gray" )  # in google_pixel_w_bars,  "w" is for workload 
## samsung bars
samsung_workload_error =  [0.025953] * len(samsung_workload)     #*****#   
label_format = '{:,.0f}'
samsung_workload[:] = [round(x, 2 ) for x in samsung_workload]
samsung_w_bars =  workload_fig.bar(y + bar_width/2, samsung_workload ,  bar_width,yerr=samsung_workload_error, label='Samsung Galaxy S8' ,  color='gray', edgecolor='black', hatch='//') 
# setting the subplot
workload_fig.set_title('Number of operations\n' + r'($\times 10e11$)', fontsize = fontsize_in_paper)
workload_fig.set_yticklabels([label_format.format(x) for x in workload_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)





################ phone energy subplot
energy_error =    [0.773] * len(google_phone_energy)     #*****#    
## google pixel bars
google_phone_energy[:] = [round(x , 2 ) for x in google_phone_energy]  # I alrady divided by 1000     
google_pixel_e_bar =  phone_energy_fig.bar(y - bar_width/2, google_phone_energy , bar_width, yerr= energy_error,  label='Google Pixel 4A 5G', color = 'gray')                             # in google_pixel_e_bars,  "e" is for energy 
## samsung bars
                                                                                         
samsung_phone_energy[:] = [round(x , 2 ) for x in samsung_phone_energy]  # I alrady divided by 1000     
samsung_e_bars =  phone_energy_fig.bar(y + bar_width/2, samsung_phone_energy ,   bar_width, yerr= energy_error, label='Samsung Galaxy S8', color='gray', edgecolor='black', hatch='//')   
## setting the subplot
phone_energy_fig.set_title('Energy consumed\n' + r'(mAh)', fontsize = fontsize_in_paper)
phone_energy_fig.set_yticklabels([label_format.format(x) for x in phone_energy_fig.get_yticks().tolist()]  ,  fontsize = fontsize_in_paper)

phone_energy_fig.legend(loc = 'upper right', prop={'size': 18})


#fig.suptitle("Workload, Energy and Energy efficiency according to the number of threads")
fig.supxlabel("Thread configurations on cores and sockets", fontsize = fontsize_in_paper)
#fig.tight_layout()
#plt.gcf().autofmt_xdate()

plt.savefig("Phone_model_influence_energy_efficiency.png")
plt.clf()
plt.cla()
plt.close()

